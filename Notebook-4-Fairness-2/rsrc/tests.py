import pandas as pd
import numpy as np

perspectives_inputs = [
    {"data": pd.DataFrame({
        "is_recid":        [0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1], 
        "decile_score":    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 5],
        "predicted_recid": [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
    })},
    {"data": pd.DataFrame({
        "is_recid":        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0], 
        "decile_score":    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 5],
        "predicted_recid": [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
    })},
    {"data": pd.DataFrame({
        "is_recid":        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
        "decile_score":    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 5],
        "predicted_recid": [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1]
    })},
]

# "name of function": [
# {"arg1": value1, "arg2": value2}, # input of first test
# {"arg1": value3, "arg2": value4}, # input of second test
# ...
# ]
inputs = {
    "foo": [
        {"col1": [2, 3], "col2": ['cats', 'dogs'], "patrick": None},
        {"col1": [10, 51, 0], "col2": ["parrots", "rabbits", "wolfs"]},
    ],
    "fpr": perspectives_inputs, 
    "fnr": perspectives_inputs,
    "ppv_complement": perspectives_inputs,
    "npv_complement": perspectives_inputs,
}

# "name of function": [ 
# (res1, res2, ...), # expected outputs of first test
# (res3, res4, ...), # expexted outputs of second test
# ...
# ]
# 
# Or: "name of variable": value_expected
expecteds = {
    "foo": [
        pd.DataFrame(data=[[2, 3], ['cats', 'dogs']]),
        pd.DataFrame(data=[[10, 51, 0], ["parrots", "rabbits", "wolfs"]]),
    ],
    "foo_var": 42,
    "accuracy_compas": 66,
    "defendants_counts": (2103, 3175),
    "fpr": (0.5, 1.0, 0.6),
    "fnr": (0.2, 0.4, 0.0),
    "ppv_complement": (1-0.5714285714285714, 1-0.8571428571428571, 1-0.14285714285714285),
    "npv_complement": (1-0.75, 1-0.0, 1-1.0),
    "sex_indicator": (1, 1, 0, 1, 0),
    "is_charge_felony_indicator": (1, 1, 1, 0, 1),
    "naive features columns": sorted(['priors_count', 'juv_fel_count', 'jail_time', 'is_charge_felony', 'is_female', 'age < 25', 'age > 45', 'African-American', 'Asian', 'Caucasian', 'Hispanic', 'Native American', 'Other']),
    "age_indicator": np.array([[0, 1], [1, 0], [0, 0], [0, 1], [1, 0]], dtype=bool).__repr__(),
    "race_indicator": np.array([[0, 1, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0], [0, 0, 1, 0, 0, 0], [1, 0, 0, 0, 0, 0], [1, 0, 0, 0, 0, 0]], dtype=bool).__repr__(),
    "features_no_race": sorted(['priors_count', 'juv_fel_count', 'jail_time', 'is_female', 'is_charge_felony', 'age < 25', 'age > 45']),
    "recid_stats": [1229, 1402, 874, 1773],
    
}


common_mistakes = {
    "accuracy_compas" : [
        "- COMPAS consider someone as a potential recidivist if its decile score is >= 5.",
        "- Use the column is_recid as ground truth."],
    "defendants_counts" : [
        "- Uncorrect races have been used.",
    ],
    "sex_indicator" : [
        "- Wrong column isolated after the use of get_dummies.",
        "- Columns must have boolean values.",
    ],
    "is_charge_felony_indicator" : [
        "- Wrong column isolated after the use of get_dummies.",
        "- Columns must have boolean values.",
    ],
    "naive features columns" : [
        "- There are missing columns.",
        "- There are duplicated columns.",
        "- Columns have wrong names.",
        "- Columns must have boolean values.",
    ],
    "age_indicator" : [
        "- Wrong column isolated after the use of get_dummies.",
        "- Columns must have boolean values.",
    ],
    "race_indicator" : [
        "- Wrong column isolated after the use of get_dummies.",
        "- Columns must have boolean values.",
    ],
}

def test(fct):
    for inp, exp in zip(inputs[fct.__name__], expecteds[fct.__name__]):
        actual = fct(**inp)
        if actual.__repr__() != exp.__repr__():
            print("❌ Tests failed... =(")
            print("--------------------------------------")
            print("On input:")
            for k, v in inp.items():
                print(f"{k}:")
                display(v)
                print()
            print("------")
            print(f"Expected:")
            display(exp)
            print(f"Actual:")
            display(actual)
            print("--------------------------------------")
            if fct.__name__ not in common_mistakes: return False 
            print("💡Here are possible mistakes that you may have done:")
            error_messages = "\n".join(common_mistakes[fct.__name__])
            print(error_messages)
            print("--------------------------------------")
            return False
    print("🆗 Tests passed ! =)") 
    return True
    
def test_values(var, var_name):
    tips = ("\n --------------------------------------\n 💡Here are possible mistakes that you may have done:\n\n"+'\n'.join(common_mistakes[var_name])) if var_name in common_mistakes else ""
    assert var == expecteds[var_name], f"❌ Test failed... =(\n\nActual: {var}\n\nExpected: {expecteds[var_name]}\n{tips}"
    print("🆗 Tests passed ! =)")
    