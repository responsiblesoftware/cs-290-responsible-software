import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from aif360.sklearn.inprocessing import ExponentiatedGradientReduction

# helper classes   

class Helper:
    train_indexes = None
    test_indexes = None
    
    def get_train_samples(df):
        return df[df.index.isin(Helper.train_indexes)] if df.index.nlevels == 1 else df.loc[df.reset_index(level=1, drop=True).index.isin(Helper.train_indexes), :]
    
    def get_test_samples(df):
        return df[df.index.isin(Helper.test_indexes)] if df.index.nlevels == 1 else df.loc[df.reset_index(level=1, drop=True).index.isin(Helper.test_indexes), :]
    
class LRModel:
    def __init__(self, max_iter=100):
        self.model = LogisticRegression(max_iter=max_iter)
        
    def fit(self, features, labels, sample_weight=None):
        return self.model.fit(Helper.get_train_samples(features), Helper.get_train_samples(labels), sample_weight=sample_weight)
    
    def score(self, features, labels):
        return self.model.score(Helper.get_test_samples(features), Helper.get_test_samples(labels))
    
    def predict(self, features, set_used=None):
        return self.model.predict(features) if set_used is None else self.model.predict(Helper.get_train_samples(features)) if set_used == "train" else self.model.predict(Helper.get_test_samples(features))

    def print_coefs(self, columns):
        return pd.DataFrame(self.model.coef_[0], index=columns, columns=['Coefficients of the model']).style.map(lambda e: f'font-weight:{"900" if abs(e) > 0.5 else ""}').apply(lambda r: ['border: solid orange 3px' if r.name == "African-American" or r.name == "Caucasian" else ''] * len(r), axis=1)
        

####################################################################################
# helper functions        
        
def disparate_impact(predictions):
    """
    Measures the disparate impact of a set of prediction.
    :param predictions: Indicator of the predicted value for each individual of a group
    """
    return predictions.sum() / predictions.shape[0]

def fit_and_display(features, labels, white_defendants, black_defendants, weights=None, model=LRModel(max_iter=1000), return_model=False):
    """
    Fit a Logistic regression model on the features and labels specified
    :param weights: Sample weights for training
    :param model: custom model
    :return_model: if True, the function returns the model after execution
    """
    if isinstance(model, ExponentiatedGradientReduction):
        model.fit(features, labels)
    else: model.fit(features, labels, sample_weight=weights)
    print(f"Accuracy of the model: {model.score(features, labels)*100}%.")
    
    white_disparate = disparate_impact(model.predict(Helper.get_test_samples(features.loc[white_defendants.index])))
    black_disparate = disparate_impact(model.predict(Helper.get_test_samples(features.loc[black_defendants.index])))
    display(pd.DataFrame({
        "White": [white_disparate], 
        "Black": [black_disparate]
    }, index=["Proportion predicted recidivist"]))
    print(f"Disparate impact ratio: {white_disparate / black_disparate}")
    
    if return_model: return model

def load_compas_data():
    global hp
    # load data
    data = pd.read_csv('https://raw.githubusercontent.com/ds-modules/HCE-Materials/main/COMPAS/compas-scores-two-years.csv')

    # drop rows known to be irrelevant
    data = data.query('days_b_screening_arrest <= 30 & days_b_screening_arrest >= -30')

    # drop columns we wont use
    data = data.drop(['compas_screening_date', 'c_case_number', 'days_b_screening_arrest', 'c_days_from_compas', 
               'r_case_number', 'r_charge_degree', 'r_days_from_arrest', 'r_offense_date', 'r_charge_desc', 'r_jail_in', 'r_jail_out',
               'violent_recid', 'is_violent_recid', 'vr_case_number', 'vr_charge_degree', 'vr_offense_date', 'vr_charge_desc', 
               'type_of_assessment', 'decile_score.1', 'score_text', 'screening_date', 
               'v_type_of_assessment', 'v_decile_score', 'v_score_text', 'v_screening_date', 
               'two_year_recid', 'start', 'end', 'event', 'priors_count.1'], axis='columns')
    
    # create the helper
    Helper.test_indexes = pd.read_csv("./rsrc/test_indexes.csv")['index']
    Helper.train_indexes = data.drop(Helper.test_indexes).index
    return data