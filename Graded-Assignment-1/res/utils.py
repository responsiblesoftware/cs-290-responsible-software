import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures


def plot_percentile_illnesses_cost(df, admitted=False):

    if admitted:
        df = df[df["Admitted"] == 1]

    percentile_df = (
        df.groupby(["Percentile", "Race"])["Active illnesses"].mean().unstack()
    )
    # Calculate the mean costs per score percentile
    cost_percentile_df = (
        df.groupby(["Percentile", "Race"])["Actual cost"].mean().unstack()
    )

    # Create subplots
    fig, axs = plt.subplots(1, 2, figsize=(15, 5))

    if not admitted:
        axs[0].axvline(97, color="black", linestyle="--", linewidth=1)
        axs[1].axvline(97, color="black", linestyle="--", linewidth=1)

    # Plot for Active Illnesses
    axs[0].plot(percentile_df.index, percentile_df["Black"], color="tab:red", marker=".", label="Black")
    axs[0].plot(
        percentile_df.index, percentile_df["White"], color="tab:blue",  marker="x", label="White"
    )
    axs[0].set_xlabel("Score Percentile")
    axs[0].set_ylabel("Active Illnesses")
    axs[0].set_title("Active Illnesses vs Score Percentiles")
    axs[0].legend()
    axs[0].grid(True)

    # Plot for Actual Cost
    axs[1].plot(
        cost_percentile_df.index,
        cost_percentile_df["Black"],
        color="tab:red",
        marker=".",
        label="Black",
    )
    axs[1].plot(
        cost_percentile_df.index,
        cost_percentile_df["White"],
        color="tab:blue",
        marker="x",
        label="White",
    )
    axs[1].set_xlabel("Score Percentile")
    axs[1].set_ylabel("Actual Cost")
    axs[1].set_title("Actual Cost vs Score Percentiles")
    axs[1].legend()
    axs[1].grid(True)
    # use log for y
    # axs[2].set_yscale("log")

    # # set minor ticks
    # def log_format(base, tick_val):
    #     # round to nearest hundred
    #     return f"{np.round(base ** tick_val, -2):.0f}"

    # axs[2].yaxis.set_major_locator(ticker.LogLocator(base=3))
    # axs[2].yaxis.set_minor_locator(ticker.LogLocator(base=3, subs=np.arange(2, 3)))
    # axs[2].yaxis.set_major_formatter(
    #     ticker.FuncFormatter(lambda y, _: log_format(3, np.log(y) / np.log(3)))
    # )

    plt.tight_layout()

    plt.show()


def plot_illnesses_cost(df):

    fig, axs = plt.subplots(1, 1, figsize=(10, 7))
    axs.scatter(
        df[df["Race"] == "White"]["Active illnesses"],
        df[df["Race"] == "White"]["Actual cost"],
        color="tab:blue",  marker="x"
    )
    axs.scatter(
        df[df["Race"] == "Black"]["Active illnesses"],
        df[df["Race"] == "Black"]["Actual cost"],
        color="tab:red", marker="o"
    )
    axs.set_xlabel("Active Illnesses")
    axs.set_ylabel("Actual Cost")
    axs.set_title("Active Illnesses vs Actual Cost")

    # plot the polynomial regression
    # Fit polynomial regression for cost vs active illnesses for white
    white_df = df[df["Race"] == "White"]
    black_df = df[df["Race"] == "Black"]

    # y_white_log = np.log(white_df["Actual cost"])
    # y_black_log = np.log(black_df["Actual cost"])

    poly = PolynomialFeatures(degree=1)
    X_white = poly.fit_transform(white_df["Active illnesses"].values.reshape(-1, 1))
    y_white = white_df["Actual cost"]
    model_white = LinearRegression().fit(X_white, y_white)

    X_black = poly.fit_transform(black_df["Active illnesses"].values.reshape(-1, 1))
    y_black = black_df["Actual cost"]
    model_black = LinearRegression().fit(X_black, y_black)

    # Generate predictions
    x_range = np.linspace(
        df["Active illnesses"].min(), df["Active illnesses"].max(), 100
    )
    X_pred = poly.fit_transform(x_range.reshape(-1, 1))
    y_pred_white = model_white.predict(X_pred)
    y_pred_black = model_black.predict(X_pred)

    # Plot the polynomial regression
    axs.plot(x_range, y_pred_black, color="tab:red", linestyle="--", label="Black Fit")
    axs.plot(x_range, y_pred_white, color="tab:blue", linestyle="--", label="White Fit")
    axs.legend()

    # axs.set_yscale("log")

    # # set minor ticks
    # def log_format(base, tick_val):
    #     # round to nearest hundred
    #     return f"{np.round(base ** tick_val, -2):.0f}"

    # axs.yaxis.set_major_locator(ticker.LogLocator(base=2))
    # axs.yaxis.set_minor_locator(ticker.LogLocator(base=2, subs=np.arange(2, 3)))
    # axs.yaxis.set_major_formatter(
    #     ticker.FuncFormatter(lambda y, _: log_format(2, np.log(y) / np.log(2)))
    # )

    plt.tight_layout()
    plt.show()


def plot_confusion_matrix(
    tp,
    tn,
    fp,
    fn,
    classifier="",
    positive_label="Heart Fail",
    negative_label="Heart Not Failed",
):
    # Create the confusion matrix
    confusion_matrix = np.array([[tn, fp], [fn, tp]])

    fig, ax = plt.subplots()
    cax = ax.matshow(confusion_matrix, cmap=plt.cm.Blues)
    plt.title("Confusion Matrix - " + classifier)
    fig.colorbar(cax)

    # Set axis labels
    ax.set_xticklabels(["", negative_label, positive_label])
    ax.set_yticklabels(["", negative_label, positive_label])

    # Label the axes
    plt.xlabel("Predicted")
    plt.ylabel("Actual")

    # Add text annotations
    for (i, j), value in np.ndenumerate(confusion_matrix):
        plt.text(j, i, f"{value}", ha="center", va="center", color="tab:red", weight="bold")

    plt.show()
