import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import MaxNLocator
import pandas as pd
import numbers as nb

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

plt.style.use("tableau-colorblind10")

def qassert(condition, message):
    if not condition:
        print(f"{bcolors.BOLD}{bcolors.FAIL}ERROR: {message}{bcolors.ENDC}")

def qround(value, decimals=None):
    if decimals is None:
        return round(value) if value is not ... else ...
    else:
        return round(value, decimals) if value is not ... else ...

def plot_queries_per_day(epfl_student_df):
    # Plot the data
    plt.figure(figsize=(12, 6))

    # Plot query number per day
    plt.plot(
        epfl_student_df["Date"],
        epfl_student_df["Number of queries"],
        marker="o",
        label="Total query number",
    )

    # Set plot labels and title
    plt.xlabel("Date")
    plt.ylabel("Total query number")
    plt.title("Total query number per day")
    ax = plt.gca()
    ax.xaxis.set_major_locator(MaxNLocator(nbins=10))
    plt.xticks(rotation=45)
    plt.legend()
    plt.grid(True)

    # Show the plot
    plt.show()


def plot_total_emissions_per_model(models_data_df):
    # Plotting with adjusted text placement
    plt.figure(figsize=(8, 4))
    plt.scatter(
        models_data_df["Total CO2e (kg)"],
        models_data_df["Accuracy (%)"],
        color=["#0072B2", "#E69F00", "#D55E00", "#56B4E9"],
        s=100,
    )

    # Annotating each point with the model name, adjusting the position
    for i, row in models_data_df.iterrows():
        plt.text(
            row["Total CO2e (kg)"] * 1.05,
            row["Accuracy (%)"],
            row["Model"],
            fontsize=12,
            ha="left",
            va="center",
        )

    # Applying logarithmic scale to the x-axis
    # plt.xscale('log')

    plt.title("Accuracy vs. Total CO2e emissions for different models")
    plt.xlabel("Total CO2e emissions (kg)")
    plt.ylabel("Accuracy (%)")
    plt.grid(True, which="both", ls="--")
    plt.show()

def print_emissions_translation(co2e_kg):
    co2_kg_to_km_car = qround(co2e_kg / 0.1639 if co2e_kg is not ... else ...)
    co2_kg_to_km_plane = qround(co2e_kg / 0.18592 if co2e_kg is not ... else ...)
    co2_kg_to_km_rail = qround(co2e_kg / 0.00446 if co2e_kg is not ... else ...)

    co2e_kg_str = f"{qround(co2e_kg):,}".replace(",", " ") if co2e_kg is not ... else "..."
    co2_kg_to_km_car_str = f"{co2_kg_to_km_car:,}".replace(",", " ") if co2_kg_to_km_car is not ... else "..."
    co2_kg_to_km_plane_str = f"{co2_kg_to_km_plane:,}".replace(",", " ") if co2_kg_to_km_plane is not ... else "..."
    co2_kg_to_km_rail_str = f"{co2_kg_to_km_rail:,}".replace(",", " ") if co2_kg_to_km_rail is not ... else "..."

    print(f"{co2e_kg_str} kg of CO₂eq are equivalent to:\n🚙 driving an average passenger car for {co2_kg_to_km_car_str} km,\n🛫 taking an international flight for {co2_kg_to_km_plane_str} km or\n🚂 travelling by train for {co2_kg_to_km_rail_str} km.")

