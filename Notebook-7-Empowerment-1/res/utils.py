import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import MaxNLocator
import pandas as pd

plt.style.use('tableau-colorblind10')

def plot_agreement_rate_group(agreement_category_df, ax=None):
    if ax is None:
        fig, ax = plt.subplots(figsize=(8, 4.5))
    agreement_category_df.plot(kind="bar", ax=ax)
    ax.set_title("Average Agreement Rate with ML Recommendations")
    ax.set_ylabel("Agreement Rate (%)")
    ax.set_xlabel("Category")
    ax.set_ylim(0, 100)
    ax.xaxis.set_tick_params(rotation=0)
    ax.grid(True)  # Show grid
    plt.tight_layout()
    return ax


def plot_agreement_rate_expertise(agreement_df, ax=None):
    if ax is None:
        fig, ax = plt.subplots(figsize=(8, 4.5))
    agreement_df.plot(kind="bar", ax=ax)
    ax.set_title("Average Agreement Rate with ML Recommendations Across Expertise")
    ax.set_ylabel("Agreement Rate (%)")
    ax.set_xlabel("Expertise")
    ax.set_ylim(0, 100)
    ax.xaxis.set_tick_params(rotation=0)
    ax.grid(True)  # Show grid
    handles, previous_labels = ax.get_legend_handles_labels()
    ax.legend(handles=handles, labels=["No recommendation shown (NoRec)", "Recommendation shown (Rec)"])
    plt.tight_layout()
    return ax


def plot_performance_group(performance_expertise_df, ax=None):
    if ax is None:
        fig, ax = plt.subplots(figsize=(8, 4.5))
    performance_expertise_df.plot(kind="bar", ax=ax)
    ax.set_title(
        "Average Performance Different Treatments and Recommendation Correctness"
    )
    ax.set_ylabel("Performance (%)")
    ax.set_xlabel("Category")
    ax.set_ylim(0, 100)
    ax.xaxis.set_tick_params(rotation=0)
    ax.grid(True)  # Show grid
    handles, previous_labels = ax.get_legend_handles_labels()
    ax.legend(handles=handles, labels=["No recommendation shown (NoRec)", "Recommendation shown (Rec)"])
    plt.tight_layout()
    return ax

def plot_performance_group(performance_expertise_df, ax=None, data_type=None):
    if data_type == "animals":
        x_label = "Recommendation from the model (Animal task)"
    elif data_type == "crowds":
        x_label = "Recommendation from the model (Crowd task)"
    else:
        x_label = "Category"
    
    if ax is None:
        fig, ax = plt.subplots(figsize=(8, 4.5))
    performance_expertise_df.plot(kind="bar", ax=ax)
    ax.set_title(
        "Average Performance Different Treatments and Recommendation Correctness"
    )
    ax.set_ylabel("Performance (%)")
    ax.set_xlabel(x_label)
    ax.set_ylim(0, 100)
    ax.xaxis.set_tick_params(rotation=0)
    ax.grid(True)  # Show grid
    handles, previous_labels = ax.get_legend_handles_labels()
    ax.legend(handles=handles, labels=["No recommendation shown (NoRec)", "Recommendation shown (Rec)"])
    plt.tight_layout()
    return ax

def plot_whole_agreement_rate_group(expertise_df):
    agreement_rates = {
        "Crowds Rec": (expertise_df["Crowds Cor Rec"] + 5 - expertise_df["Crowds Incor Rec"]) / 10 * 100,
        "Animals Rec": (expertise_df["Animals Cor Rec"] + 5 - expertise_df["Animals Incor Rec"]) / 10 * 100,
        "Crowds NoRec": (expertise_df["Crowds Cor NoRec"] + 5 - expertise_df["Crowds Incor NoRec"]) / 10 * 100,
        "Animals NoRec": (expertise_df["Animals Cor NoRec"] + 5 - expertise_df["Animals Incor NoRec"]) / 10 * 100,
    }

    # Compute the mean across the "Beginner" and "Expert" rows
    agreement_category_df_temp = pd.DataFrame(agreement_rates).mean()

    # Organize the results into the final DataFrame
    agreement_category_df = pd.DataFrame({
        "NoRec": [
            agreement_category_df_temp["Animals NoRec"],
            agreement_category_df_temp["Crowds NoRec"]
        ],
        "Rec": [
            agreement_category_df_temp["Animals Rec"],
            agreement_category_df_temp["Crowds Rec"]
        ]
    }, index=["Animals", "Crowds"])
    
    plot_agreement_rate_group(agreement_category_df)
    
    return agreement_category_df

def plot_whole_performance_group(df):
    performance_category_df = pd.DataFrame({
        "NoRec": [
            df["Animals Cor NoRec"].mean() * 100 / 5,
            df["Animals Incor NoRec"].mean() * 100 / 5,
            df["Crowds Cor NoRec"].mean() * 100 / 5,
            df["Crowds Incor NoRec"].mean() * 100 / 5
        ],
        "Rec": [
            df["Animals Cor Rec"].mean() * 100 / 5,
            df["Animals Incor Rec"].mean() * 100 / 5,
            df["Crowds Cor Rec"].mean() * 100 / 5,
            df["Crowds Incor Rec"].mean() * 100 / 5
        ]
    }, index=["Animals Cor", "Animals Incor", "Crowds Cor", "Crowds Incor"])
    
    plot_performance_group(performance_category_df)
    
    return performance_category_df

def plot_animal_all_subplots(df, expertise_df):
    agreement_expertise_df = pd.DataFrame({
        "NoRec": (expertise_df["Animals Cor NoRec"] + 5 - expertise_df["Animals Incor NoRec"]) / 10 * 100,
        "Rec": (expertise_df["Animals Cor Rec"] + 5 - expertise_df["Animals Incor Rec"]) / 10 * 100
    }, index=["Beginner", "Expert"])
    performance_category_df = pd.DataFrame({
        "NoRec": [
            df["Animals Cor NoRec"].mean() * 100 / 5,
            df["Animals Incor NoRec"].mean() * 100 / 5
        ],
        "Rec": [
            df["Animals Cor Rec"].mean() * 100 / 5,
            df["Animals Incor Rec"].mean() * 100 / 5
        ]
    }, index=["Correct", "Incorrect"])
    performance_incorrect_df = pd.DataFrame({
        "NoRec": expertise_df["Animals Incor NoRec"] / 5 * 100,
        "Rec": expertise_df["Animals Incor Rec"] / 5 * 100
    }, index=["Beginner", "Expert"])
    
    plot_all_subplots(agreement_expertise_df, performance_category_df, performance_incorrect_df, data_type="animals")

    # # Compute the mean across the "Beginner" and "Expert" rows
    # agreement_category_df_temp = pd.DataFrame(agreement_rates).mean()

    # # Organize the results into the final DataFrame
    # agreement_category_df = pd.DataFrame({
    #     "NoRec": [
    #         agreement_category_df_temp["Crowds NoRec"]
    #     ],
    #     "Rec": [
    #         agreement_category_df_temp["Crowds Rec"]
    #     ]
    # }, index=["Crowds"])
    

def plot_performance_incorrect(performance_incorrect_df, ax=None):
    if ax is None:
        fig, ax = plt.subplots(figsize=(8, 4.5))
    performance_incorrect_df.plot(kind="bar", ax=ax)
    ax.set_title(
        "Average Performance for Questions with Incorrect Recommendation Across Expertise"
    )
    ax.set_ylabel("Performance (%)")
    ax.set_xlabel("Expertise")
    ax.set_ylim(0, 100)
    ax.xaxis.set_tick_params(rotation=0)
    ax.grid(True)  # Show grid
    handles, previous_labels = ax.get_legend_handles_labels()
    ax.legend(handles=handles, labels=["No recommendation shown (NoRec)", "Recommendation shown (Rec)"])
    plt.tight_layout()
    return ax


def plot_all_subplots(agreement_expertise_df, performance_category_df, performance_incorrect_df, data_type=None):
    fig, axs = plt.subplots(2, 2, figsize=(16, 9))

    # Reuse the individual plot functions
    plot_performance_group(performance_category_df, ax=axs[0, 0], data_type=data_type)
    plot_agreement_rate_expertise(agreement_expertise_df, ax=axs[1, 0])
    plot_performance_incorrect(performance_incorrect_df, ax=axs[0, 1])

    # Hide the fourth subplot
    axs[1, 1].axis('off')

    plt.tight_layout()
    plt.show()