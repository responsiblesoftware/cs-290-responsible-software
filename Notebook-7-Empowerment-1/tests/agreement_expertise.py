from otter.test_files import test_case

OK_FORMAT = False

name = "agreement_expertise"
points = None

@test_case(points=None, hidden=False)
def test_agreement_expertise_df(pd, pdt, agreement_crowds_expertise_df):
    agreement_expertise_df_solution = pd.DataFrame({
        'NoRec': [48.9333, 50.1333],
        'Rec': [ 80.2666, 71.3333]
    }, index=['Beginner', 'Expert'])
    pdt.assert_frame_equal(agreement_expertise_df_solution, agreement_crowds_expertise_df, atol=1e-4)
    
