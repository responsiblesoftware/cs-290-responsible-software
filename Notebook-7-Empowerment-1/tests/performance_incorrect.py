from otter.test_files import test_case

OK_FORMAT = False

name = "performance_incorrect"
points = None

@test_case(points=None, hidden=False)
def test_performance_incorrect_df(pd, pdt, performance_crowds_incorrect_df):
    performance_incorrect_df_solution = pd.DataFrame({
        'NoRec': [72.8, 66.1333],
        'Rec': [ 29.8666, 48.0]
    }, index=['Beginner', 'Expert'])
    pdt.assert_frame_equal(performance_incorrect_df_solution, performance_crowds_incorrect_df, atol=1e-4)
    
