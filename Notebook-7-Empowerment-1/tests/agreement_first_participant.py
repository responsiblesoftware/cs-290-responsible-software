from otter.test_files import test_case

OK_FORMAT = False

name = "agreement_first_participant"
points = None

@test_case(points=None, hidden=False)
def test_first_participant_agreement_rate(first_particpant_agreement_no_rec, first_particpant_agreement_rec, math):
	assert math.isclose(first_particpant_agreement_no_rec, 30.0)
	assert math.isclose(first_particpant_agreement_rec, 80.0)

