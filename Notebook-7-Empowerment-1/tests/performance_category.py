from otter.test_files import test_case

OK_FORMAT = False

name = "performance_category"
points = None

@test_case(points=None, hidden=False)
def test_performance_category_df(pd, pdt, performance_crowds_category_df):
    performance_category_df_solution = pd.DataFrame({
        'NoRec': [68.5333, 69.4666],
        'Rec': [90.5333, 38.9333]
    }, index=['Correct', 'Incorrect'])
    pdt.assert_frame_equal(performance_category_df_solution, performance_crowds_category_df, atol=1e-4)

