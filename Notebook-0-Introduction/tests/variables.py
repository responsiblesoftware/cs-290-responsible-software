from otter.test_files import test_case

OK_FORMAT = False

name = "variables"
points = None

@test_case(points=None, hidden=False)
def test_option_1(option_1):
    assert option_1 == "1. Swerve"

@test_case(points=None, hidden=False)
def test_option_2(option_2):
    assert option_2 == "2. Do nothing"

@test_case(points=None, hidden=False)
def test_choice(choice):
    assert choice in [1, 2]

@test_case(points=None, hidden=False)
def test_swerve(swerve):
    assert type(swerve) == bool
    
