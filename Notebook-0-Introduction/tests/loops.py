from otter.test_files import test_case

OK_FORMAT = False

name = "loops"
points = None


@test_case(points=None, hidden=False)
def run_survey_multiple_scenarios_for_loop_has_correct_range(
    run_survey_multiple_scenarios, patch
):
    with patch(
        "builtins.input", side_effect=["1", "1", "1", "1", "1", "1", "1", "1", "1"]
    ), patch(
        "__main__.display_selected_scenario"
    ) as mock_display_selected_scenario, patch(
        "builtins.print"
    ) as mock_print:
        run_survey_multiple_scenarios()
    assert mock_display_selected_scenario.call_count == 9


@test_case(points=None, hidden=False)
def run_survey_multiple_scenarios_calls_display_selected_scenario_called_with_correct_args(
    patch, run_survey_multiple_scenarios, call
):
    with patch(
        "builtins.input", side_effect=["1", "1", "1", "1", "1", "1", "1", "1", "1"]
    ), patch(
        "__main__.display_selected_scenario"
    ) as mock_display_selected_scenario, patch(
        "builtins.print"
    ) as mock_print:
        run_survey_multiple_scenarios()
    assert mock_display_selected_scenario.call_args_list == [
        call(0),
        call(1),
        call(2),
        call(3),
        call(4),
        call(5),
        call(6),
        call(7),
        call(8),
    ]


@test_case(points=None, hidden=False)
def test_get_choice_improved(get_choice_improved, patch):
    with patch("builtins.input", side_effect=["1"]):
        assert get_choice_improved() == 1
    with patch("builtins.input", side_effect=["2"]):
        assert get_choice_improved() == 2
    with patch("builtins.input", side_effect=["s", "1"]), patch(
        "builtins.print"
    ) as mock_print:
        assert get_choice_improved() == 1
