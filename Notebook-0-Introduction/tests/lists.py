from otter.test_files import test_case

OK_FORMAT = False

name = "lists"
points = None


@test_case(points=None, hidden=False)
def run_survey_multiple_scenarios_returns_correct_list(
    run_survey_multiple_scenarios, patch
):
    with patch("builtins.print") as mock_print, patch(
        "builtins.input", side_effect=["2", "1", "1", "2", "2", "1", "2", "1", "2"]
    ):
        assert run_survey_multiple_scenarios() == [2, 1, 1, 2, 2, 1, 2, 1, 2]


@test_case(points=None, hidden=False)
def test_responses_of_participant_42(responses_of_participant_42):
    assert responses_of_participant_42 == [1, 2, 1, 2, 2, 1, 1, 2, 1]


@test_case(points=None, hidden=False)
def test_response_of_participant_42_scenario_0(response_of_participant_42_scenario_0):
    assert response_of_participant_42_scenario_0 == 1
