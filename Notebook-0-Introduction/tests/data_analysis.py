from otter.test_files import test_case

OK_FORMAT = False

name = "data_analysis"
points = None

@test_case(points=None, hidden=False)
def data_CH_is_correct(pandas, data_CH):
    data_CH_new = data_CH
    data_CH_reference = pandas.read_csv("res/moral_machine_data_CH.csv")
    data_CH_new.reset_index(inplace=True, drop=True)
    data_CH_reference.reset_index(inplace=True, drop=True)
    assert data_CH_reference.equals(data_CH_new)


@test_case(points=None, hidden=False)
def total_responses_for_species_scenario_is_correct(
    total_responses_for_species_scenario,
):
    assert total_responses_for_species_scenario == 823


@test_case(points=None, hidden=False)
def sparing_humans_percentage_is_correct(math, rounded_percent_responses_sparing_humans):
    assert math.isclose(rounded_percent_responses_sparing_humans, 86.15)


@test_case(points=None, hidden=False)
def sparing_high_status_percentage_rounded_is_correct(
    rounded_percent_responses_sparing_high_status,
):
    assert rounded_percent_responses_sparing_high_status == 72.83


