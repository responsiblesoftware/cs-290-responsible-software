from otter.test_files import test_case

OK_FORMAT = False

name = "functions"
points = None


@test_case(points=None, hidden=False)
def test_run_survey(run_survey, patch):
    with patch("builtins.input", return_value="1"), patch(
        "builtins.print"
    ) as mock_print:
        response_1 = run_survey()
    with patch("builtins.input", return_value="2"), patch(
        "builtins.print"
    ) as mock_print:
        response_2 = run_survey()
    with patch("builtins.input", return_value="three"), patch(
        "builtins.print"
    ) as mock_print:
        response_3 = run_survey()
    assert response_1 == (
        1,
        "The autopilot swerves and avoids the pedestrians in front of the vehicle.",
    )
    assert response_2 == (
        2,
        "The autopilot does nothing and avoids the pedestrians in the other lane.",
    )
    assert response_3 == ("three", "Invalid choice")


@test_case(points=None, hidden=False)
def test_handle_participant_response(handle_participant_response):
    assert (
        handle_participant_response(1)
        == "The autopilot swerves and avoids the pedestrians in front of the vehicle."
    )
    assert (
        handle_participant_response(2)
        == "The autopilot does nothing and avoids the pedestrians in the other lane."
    )
    assert handle_participant_response(3) == "Invalid choice"
