from otter.test_files import test_case

OK_FORMAT = False

name = "conditionals"
points = None


@test_case(points=None, hidden=False)
def outcome_corresponds_to_choice(outcome, choice):
    if choice == 1:
        assert (
            outcome
            == "The autopilot swerves and avoids the pedestrians in front of the vehicle."
        )
    elif choice == 2:
        assert (
            outcome
            == "The autopilot does nothing and avoids the pedestrians in the other lane."
        )


@test_case(points=None, hidden=False)
def outcome_corresponds_to_swerve_value(outcome, swerve):
    if swerve:
        assert (
            outcome
            == "The autopilot swerves and avoids the pedestrians in front of the vehicle."
        )
    else:
        assert (
            outcome
            == "The autopilot does nothing and avoids the pedestrians in the other lane."
        )
