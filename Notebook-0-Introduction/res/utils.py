import textwrap
import time
from prettytable import PrettyTable
import pandas
import numpy as np


def get_choice():
    choice = input("What is your choice? (Select 1 or 2 and press Enter.)")
    try:
        return int(choice)
    except ValueError:
        return choice


def display_selected_scenario(index):
    print(
        "\n\n"
        + scenarios[index][0]
        + "\n======================================================================\n"
        + textwrap.fill(scenarios[index][1])
        + "\n----------------------------------------------------------------------\n"
        + "1. Swerve\n2. Do nothing\n"
        + "======================================================================\n"
    )
    time.sleep(0.1)


def display_results(results):
    for i in range(len(results)):
        if results[i] == 1:
            print(
                "For scenario",
                i,
                "(",
                scenarios[i][0],
                ") you decided that the autopilot should swerve.",
            )
        else:
            print(
                "For scenario",
                i,
                "(",
                scenarios[i][0],
                ") you decided that the autopilot should do nothing.",
            )


def display_analysis_results(results):
    table = PrettyTable(
        ["Scenario", "Autopilot should swerve", "Autopilot should do nothing"]
    )
    for i in range(len(results)):
        table.add_row(
            [scenarios[i][0], f"{str(results[i])}%", f"{str(100 - results[i])}%"]
        )
    print(table)


# scenarios inspired by https://learning.hccs.edu/faculty/david.poston/phil1301.80361/readings-for-march-31/JJ%20Thomson%20-%20Killing-%20Letting%20Die-%20and%20the%20Trolley%20Problem.pdf and https://neal.fun/absurd-trolley-problems/
# Data in last scenario taken from https://www.carvertical.com/en/blog/how-much-co2-does-an-electric-car-emit and https://www.nature.com/articles/s41467-021-24487-w
scenarios = [
    [
        "🚎 Classic",
        "A self-driving car with a brake failure is heading towards five pedestrians crossing the street. The car can swerve to other lane, hitting one pedestrian instead. What should the autopilot do?",
    ],
    [
        "👶 Age",
        "A self-driving car with a brake failure is heading towards five elderly people crossing the street. The car can swerve to other lane, hitting one child instead. What should the autopilot do?",
    ],
    [
        "👷‍♀️ Personal responsibility",
        "A self-driving car with a brake failure is heading towards five workers repairing the street; they have been warned about the dangers of the job and they are paid high salaries to compensate. The car can swerve to other lane, hitting one pedestrian instead. What should the autopilot do?",
    ],
    [
        "🚫 Breaking the rules",
        "A self-driving car with a brake failure is heading towards five workers repairing the street; they have been warned about the dangers of the job and they are paid high salaries to compensate. The car can swerve to other lane, hitting a pedestrian, who ignored the red light and is crossing illegally. What should the autopilot do?",
    ],
    [
        "💰 Social status",
        "A self-driving car with a brake failure is heading towards a CEO crossing the street. The car can swerve to other lane, hitting one homeless person instead. What should the autopilot do?",
    ],
    [
        "😴 Avoiding suffering",
        "A self-driving car with a brake failure is heading towards one person crossing the street. This person is sleepwalking and will not feel any pain. The car can swerve to other lane, hitting one awake person instead. What should the autopilot do?",
    ],
    [
        "😸 Pets",
        "A self-driving car with a brake failure is heading towards five pedestrians crossing the street. The car can swerve to other lane, hitting one cat instead. What should the autopilot do?",
    ],
    [
        "🤖 Robots",
        "A self-driving car with a brake failure is heading towards five sentient robots crossing the street. The car can swerve to other lane, hitting one human instead. What should the autopilot do?",
    ],
    [
        "🌍 Environment",
        "An electric self-driving car is releasing about one ton of CO2 per year, which will kill five people over 20 years. The autopilot can swerve, hitting a wall and destroying the car (there is no driver, so no one will be harmed in this case). What should the autopilot do?",
    ],
]

participant_id_to_response = {
    0: [1, 2, 2, 2, 1, 1, 2, 2, 2],
    1: [1, 2, 2, 2, 1, 1, 2, 2, 1],
    2: [2, 2, 1, 2, 1, 2, 1, 2, 1],
    3: [1, 2, 1, 2, 1, 1, 1, 2, 1],
    4: [2, 2, 2, 1, 2, 1, 2, 2, 2],
    5: [1, 2, 1, 2, 2, 1, 1, 2, 1],
    6: [1, 1, 1, 2, 1, 2, 1, 1, 1],
    7: [1, 1, 1, 1, 2, 1, 1, 2, 1],
    8: [1, 2, 1, 2, 1, 1, 1, 2, 1],
    9: [1, 2, 1, 1, 2, 1, 1, 1, 1],
    10: [1, 1, 1, 2, 1, 2, 1, 1, 1],
    11: [1, 2, 1, 2, 2, 1, 1, 2, 1],
    12: [1, 2, 2, 2, 1, 1, 1, 2, 1],
    13: [1, 1, 1, 2, 2, 2, 1, 2, 1],
    14: [1, 2, 1, 2, 1, 2, 2, 2, 2],
    15: [1, 1, 1, 2, 2, 1, 1, 2, 1],
    16: [1, 2, 1, 1, 1, 2, 1, 2, 1],
    17: [2, 2, 1, 2, 2, 1, 1, 2, 1],
    18: [1, 2, 1, 2, 2, 2, 1, 2, 2],
    19: [1, 2, 1, 2, 2, 1, 1, 2, 2],
    20: [1, 1, 2, 2, 1, 2, 1, 1, 1],
    21: [1, 2, 1, 2, 1, 2, 1, 1, 1],
    22: [1, 1, 1, 2, 1, 2, 1, 2, 2],
    23: [1, 2, 1, 1, 1, 1, 1, 2, 1],
    24: [1, 1, 1, 2, 1, 1, 1, 2, 1],
    25: [2, 2, 1, 1, 1, 1, 1, 2, 1],
    26: [1, 2, 1, 1, 1, 1, 1, 2, 1],
    27: [1, 2, 1, 1, 1, 1, 1, 2, 1],
    28: [1, 2, 1, 2, 1, 1, 2, 2, 2],
    29: [1, 1, 2, 2, 2, 1, 1, 2, 1],
    30: [2, 2, 1, 2, 1, 1, 1, 2, 1],
    31: [1, 2, 1, 2, 2, 2, 1, 2, 2],
    32: [1, 1, 2, 2, 2, 2, 1, 2, 1],
    33: [1, 2, 2, 1, 1, 1, 1, 1, 1],
    34: [1, 2, 1, 2, 2, 2, 2, 2, 1],
    35: [1, 1, 1, 1, 2, 2, 1, 2, 2],
    36: [2, 1, 2, 2, 2, 2, 1, 2, 2],
    37: [1, 2, 1, 2, 1, 1, 2, 2, 2],
    38: [2, 2, 1, 2, 1, 1, 1, 2, 2],
    39: [1, 2, 1, 2, 2, 1, 1, 2, 1],
    40: [1, 2, 1, 2, 2, 1, 1, 1, 2],
    41: [1, 2, 1, 2, 1, 2, 1, 1, 1],
    42: [1, 2, 1, 2, 2, 1, 1, 2, 1],
    43: [1, 2, 1, 2, 1, 2, 1, 1, 2],
    44: [1, 2, 1, 1, 1, 2, 1, 2, 2],
    45: [2, 2, 1, 2, 1, 2, 1, 2, 1],
    46: [1, 2, 2, 1, 2, 1, 1, 1, 2],
    47: [2, 2, 1, 2, 1, 2, 1, 2, 2],
    48: [1, 2, 2, 2, 1, 1, 1, 2, 1],
    49: [1, 2, 1, 2, 1, 2, 2, 2, 1],
    50: [1, 2, 1, 2, 2, 1, 1, 2, 1],
    51: [2, 2, 1, 2, 2, 1, 2, 2, 1],
    52: [2, 2, 1, 2, 2, 2, 1, 2, 2],
    53: [1, 2, 2, 2, 1, 2, 1, 2, 1],
    54: [2, 2, 1, 2, 2, 2, 1, 2, 1],
    55: [1, 2, 1, 1, 1, 1, 1, 1, 1],
    56: [2, 2, 1, 2, 1, 1, 1, 1, 1],
    57: [2, 1, 1, 1, 2, 2, 1, 2, 2],
    58: [1, 2, 1, 2, 1, 1, 1, 2, 1],
    59: [1, 1, 2, 2, 2, 1, 1, 2, 1],
    60: [2, 2, 1, 2, 2, 1, 1, 2, 1],
    61: [1, 1, 1, 1, 2, 2, 2, 1, 2],
    62: [1, 1, 1, 1, 2, 2, 1, 2, 1],
    63: [1, 2, 2, 2, 1, 1, 1, 2, 2],
    64: [1, 2, 1, 2, 2, 2, 1, 2, 1],
    65: [1, 1, 1, 2, 1, 1, 1, 2, 1],
    66: [1, 1, 1, 2, 1, 1, 1, 2, 1],
    67: [1, 2, 2, 2, 2, 1, 1, 2, 1],
    68: [1, 2, 2, 2, 2, 2, 2, 2, 1],
    69: [1, 1, 1, 2, 1, 2, 1, 2, 1],
    70: [1, 1, 1, 2, 2, 2, 1, 2, 1],
    71: [1, 2, 2, 2, 1, 1, 1, 2, 1],
    72: [2, 2, 1, 2, 1, 2, 2, 2, 2],
    73: [1, 1, 2, 1, 2, 2, 1, 2, 1],
    74: [1, 2, 1, 2, 2, 1, 1, 2, 1],
    75: [1, 2, 1, 2, 2, 2, 1, 2, 1],
    76: [1, 2, 1, 2, 2, 1, 1, 2, 2],
    77: [1, 2, 1, 2, 1, 2, 1, 2, 1],
    78: [2, 2, 1, 1, 2, 1, 1, 2, 1],
    79: [1, 2, 1, 2, 1, 1, 1, 2, 2],
    80: [1, 2, 1, 2, 2, 2, 1, 2, 2],
    81: [1, 1, 1, 2, 2, 1, 1, 2, 2],
    82: [1, 2, 2, 2, 1, 2, 1, 2, 2],
    83: [2, 2, 1, 2, 1, 1, 1, 2, 2],
    84: [2, 2, 1, 2, 1, 2, 1, 2, 2],
    85: [1, 2, 1, 1, 1, 2, 1, 2, 1],
    86: [1, 2, 1, 1, 2, 1, 1, 2, 2],
    87: [1, 2, 1, 2, 1, 1, 1, 2, 1],
    88: [1, 2, 1, 2, 1, 2, 1, 2, 2],
    89: [1, 2, 1, 2, 2, 2, 1, 2, 1],
    90: [1, 2, 1, 2, 1, 1, 1, 2, 2],
    91: [1, 2, 1, 2, 2, 2, 1, 2, 2],
    92: [1, 2, 1, 2, 2, 2, 2, 2, 1],
    93: [1, 2, 1, 2, 2, 2, 1, 2, 2],
    94: [1, 2, 2, 2, 2, 2, 1, 2, 1],
    95: [2, 2, 1, 2, 1, 2, 2, 2, 2],
    96: [1, 2, 1, 2, 2, 1, 1, 2, 1],
    97: [2, 2, 1, 1, 1, 1, 1, 2, 1],
    98: [1, 2, 1, 2, 1, 1, 1, 2, 1],
    99: [1, 1, 2, 2, 1, 1, 2, 2, 1],
}


def get_scenario_data_for_country(data, country, scenario_type):
    data["Chosen group"] = np.where(
        data["Group saved"] == 1,
        data["Pedestrians (Group 1)"],
        data["Pedestrians (Group 2)"],
    )
    data_country = data[data["Respondent country"] == country]
    match scenario_type:
        case "Species":
            attributes = ["Humans", "Pets"]
        case "Social Status":
            attributes = ["High", "Low"]
        case "Fitness":
            attributes = ["Fit", "Fat"]
        case "Age":
            attributes = ["Young", "Old"]
        case "Gender":
            attributes = ["Male", "Female"]
        case "Utilitarian":
            attributes = ["More", "Less"]
    percentage = round(
        data_country["Chosen group"].value_counts()[attributes[0]]
        / data_country["Scenario type"].value_counts()[scenario_type]
        * 100,
        2,
    )
    return pandas.DataFrame(
        [
            [country, attributes[0], percentage],
            [country, attributes[1], 100 - percentage],
        ],
        columns=["Country", "Group spared", "Percentage"],
    )


def get_scenario_data_for_countries(data, country1, country2, scenario_type):
    try:
        scenario_country1 = get_scenario_data_for_country(data, country1, scenario_type)
        scenario_country2 = get_scenario_data_for_country(data, country2, scenario_type)
        df = pandas.concat([scenario_country1, scenario_country2])
        return df.pivot(index="Group spared", columns="Country", values="Percentage")
    except KeyError:
        print("One or both countries do not have data for the specified scenario type.")
