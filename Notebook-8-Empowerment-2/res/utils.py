# import libraries
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.ensemble import GradientBoostingClassifier
import shap
import shap.plots.colors as color
shap.DeepExplainer

plt.style.use("tableau-colorblind10")
sns.set_palette("colorblind")


def load_dataset_eda():
    X_display,y_display = shap.datasets.adult(display=True)
    
    X_display['Loan granted'] = y_display
    
    # keep only samples from the United States (removes ~ 10% samples)
    X_us = X_display[X_display['Country'] == ' United-States']
    X_us.drop(columns=['Country'], inplace = True)
    
    return X_us

def load_dataset_model():
    X,y = shap.datasets.adult(display=False)
    
    X['Loan granted'] = y
    
    # keep only samples from the United States (removes ~ 10% samples)
    X_us = X[X['Country'] == 39] # 39 = code for US in shap dataset
    X_us.drop(columns=['Country'], inplace = True)
    
    return X_us

def plot_feature_importance_analysis(interpret_model, X_train):
    # Feature importance for the interpretable model
    coefficients = np.abs(interpret_model.coef_[0])
    feature_names = interpret_model.feature_names_in_


    # Plot feature importance
    fig,axs = plt.subplots(1, 1, figsize=(7, 3), sharex = True)
    axs.barh(feature_names, coefficients, color='skyblue')
    axs.set_title('Logistic Regression Model Feature Importance')
    axs.set_xlabel('Importance Score')
    axs.set_ylabel('Features')
    axs.grid(axis='x')

    plt.tight_layout()
    plt.show()

def debugging_with_shap(features_to_remove, id_sample_to_explain):
    
    if type(features_to_remove) != type([]):
        print("Error: features_to_remove should be a list...")
    else:
        df = load_dataset_model()
        # separate the features and labels
        features = ['Age', 'Workclass', 'Education-Num', 'Marital Status', 'Occupation', 'Relationship', 'Race', 'Sex', 'Capital Gain', 'Capital Loss', 'Hours per week']
        label = '>50k'

        X = df[features]
        y = df[label].tolist()

        # Split data into train and test sets
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=7)
        
        new_features = [feat for feat in X_train.columns if feat not in features_to_remove]
        
        # train complex model (Gradient Boosting)
        complex_model = GradientBoostingClassifier(random_state=42)
        complex_model.fit(X_train[new_features], y_train)

        # evaluate 
        complex_pred = complex_model.predict(X_test[new_features])

        # retrieve the False Negatives
        df_1_3 = X_test.copy()
        df_1_3['y_test'] = y_test
        df_1_3['y_complex'] = complex_pred
        
        df_FN = df_1_3[np.logical_and(df_1_3['y_test'] == 1, df_1_3['y_complex'] == 0)]
        if id_sample_to_explain >= len(df_FN):
            print(f"Error: id_sample_to_explain should be lower than {len(df_FN)}")
        else:
            # define an explainer
            explainer = shap.TreeExplainer(complex_model)

            # compute the shapley values
            shap_values = explainer(df_FN[new_features])

            # print baseline
            print("Baseline:\nAccuracy on test set: 86.44%\nNumber of False Negatives: 547\n")
            # compute accuracy
            accuracy = accuracy_score(y_test, complex_pred)
            
            # compute confusion matrix
            cm = confusion_matrix(y_test, complex_pred)
            
            # print simulation results  
            print(f"Your simulation:\nAccuracy on test set: {100*accuracy:.2f}%\nNumber of False Negatives: {cm[1][0]}\n")

            plt.figure(figsize=(12,7))
            plt.subplot(2,1,1)
            # Insert first SHAP plot here
            shap.plots.waterfall(shap_values[id_sample_to_explain], max_display=20, show=False)
            #shap.plots.bar(shap_values)
            plt.title("Waterfall plot for one sample")
            plt.subplot(2,1,2)
            # Insert second SHAP plot here
            sns.heatmap(cm, annot=True, fmt="d", cmap='viridis', annot_kws={"size": 16})
            plt.title("Confusion matrix")
            plt.xlabel('Predicted')
            plt.ylabel('True')
            plt.tight_layout()
            plt.show()

def load_df_FN(X_test, y_test, y_complex):
    df_1_3 = X_test.copy()
    df_1_3['y_test'] = y_test
    df_1_3['y_complex'] = y_complex
    df_FN = df_1_3[np.logical_and(df_1_3['y_test'] == 1, df_1_3['y_complex'] == 0)]
    return df_FN

def plot_confusion_matrices(cm_simple, cm_complex):
    
    fig, axs = plt.subplots(1, 2, figsize = (12,5))
    # confusion matrix for simple model
    sns.heatmap(cm_simple, annot=True, fmt="d", cmap='viridis', annot_kws={"size": 16}, ax = axs[0])
    axs[0].set_title("Logistic regression model")
    axs[0].set_xlabel('Predicted')
    axs[0].set_ylabel('True')
    # confusion matrix for complex model
    sns.heatmap(cm_complex, annot=True, fmt="d", cmap='viridis', annot_kws={"size": 16}, ax = axs[1])
    axs[1].set_title("Neural network model")
    axs[1].set_xlabel('Predicted')
    axs[1].set_ylabel('True')

    plt.suptitle("Confusion matrices")
    plt.tight_layout()
    plt.show()

def plot_shap_correct_vs_incorrect(complex_model, X_test, y_test, y_complex):
    
    X1 = X_test.copy()
    X1['y_test'] = y_test
    X1['y_complex'] = y_complex

    X_TP = X1[np.logical_and(X1['y_test'] == 1, X1['y_complex'] == 1)]
    X_TN = X1[np.logical_and(X1['y_test'] == 0, X1['y_complex'] == 0)]
    X_FP = X1[np.logical_and(X1['y_test'] == 0, X1['y_complex'] == 1)]
    X_FN = X1[np.logical_and(X1['y_test'] == 1, X1['y_complex'] == 0)]

    # define an explainer
    explainer = shap.TreeExplainer(complex_model)

    X_correct = pd.concat([X_TP, X_TN])   # concatenate the correct predictions from TP and TN
    X_incorrect = pd.concat([X_FP, X_FN]) # concatenate the incorrect predictions from FP and FN
    # remove columns 'y_test' and 'y_complex'
    X_correct = X_correct.drop(['y_test', 'y_complex'], axis = 1)
    X_incorrect = X_incorrect.drop(['y_test', 'y_complex'], axis = 1)
    
    shap_values_correct = explainer(X_correct) 
    shap_values_incorrect = explainer(X_incorrect) 
    
    plt.figure(figsize=(12,8))
    plt.subplot(2,1,1)
    # Insert first SHAP plot here
    shap.plots.bar(shap_values_correct, show = False) 
    #shap.plots.beeswarm(shap_values_correct, max_display=20, show = False)
    plt.title("Correct predictions")
    plt.subplot(2,1,2)
    # Insert second SHAP plot here
    shap.plots.bar(shap_values_incorrect, show = False) 
    #shap.plots.beeswarm(shap_values_incorrect, max_display=20, show = False)
    plt.title("Incorrect predictions")

    plt.tight_layout()
    plt.show()

def simulation_customer(customer_id, complex_model, shap_values, explainer, X_test, feature_to_change, new_value):

    # code to manage wrong name of feature or wrong value of feature
    if feature_to_change not in X_test.columns:
        print(f"Error: feature {feature_to_change} not possible.\nPossible features are: {X_test.columns.tolist()}")
        return

    if feature_to_change in ['Education-Num', 'Workclass', 'Marital Status', 'Occupation', 'Relationship', 'Sex', 'Race'] :
        if new_value not in X_test[feature_to_change].unique():
            print(f"Error: value {new_value} not possible for feature {feature_to_change}.\nCan only be: {np.sort(X_test[feature_to_change].unique())}")
            return
    else:
        if (new_value < np.min(X_test[feature_to_change].unique().tolist())) or (new_value > np.max(X_test[feature_to_change].unique().tolist())):
            print(f"Error: value {new_value} not possible for feature {feature_to_change}.\nCan only be in the interval: [{np.min(X_test[feature_to_change].unique().tolist()), np.max(X_test[feature_to_change].unique().tolist())}]")
            return

    X_test_modified = X_test.copy()
    X_test_modified.reset_index(drop=True, inplace=True)
    row_customer = X_test_modified.iloc[[customer_id]]

    # copy old values of customer
    # new_values = row_customer.values.tolist()[0]
    # change value of Education-Num
    # new_values[2] = new_value

    # modify in dataframe
    # X_test_modified.iloc[36] = np.array(new_values)
    # row_customer_modified = X_test_modified.iloc[[36]]

    X_test_modified.loc[customer_id, feature_to_change] = new_value

    row = pd.DataFrame([X_test_modified.iloc[customer_id]])

    # explainer = shap.Explainer(complex_model.predict)
    # compute the shap values
    shap_value = explainer(row)[0]

    shap_value.values = shap_value.values.squeeze()

    # plot the shap values for the customer
    shap.plots.waterfall(shap_value, max_display=20)


def plot_sex_fairness_comparision(df, shap_values, X_test, features):
    # Manually create cohorts based on the original 'Sex' column
    sex_cohort = [sex for sex in df.loc[X_test.index, 'Sex']]

    # Create a DataFrame for SHAP values and corresponding cohorts
    shap_df = pd.DataFrame(shap_values.values, columns=features)
    shap_df['Sex'] = sex_cohort.copy()

    # Compute the mean absolute SHAP values for each feature, separated by cohort
    mean_abs_shap_values_women = shap_df[shap_df['Sex'] == 0].abs().mean()
    mean_abs_shap_values_men = shap_df[shap_df['Sex'] == 1].abs().mean()

    # Drop the 'Sex' column to focus only on feature importance
    mean_abs_shap_values_women = mean_abs_shap_values_women.drop('Sex')
    mean_abs_shap_values_men = mean_abs_shap_values_men.drop('Sex')

    # Create a combined DataFrame for plotting
    comparison_df = pd.DataFrame({
        'Women': mean_abs_shap_values_women,
        'Men': mean_abs_shap_values_men
    }).reset_index()

    comparison_df = comparison_df.melt(id_vars="index", var_name="Cohort", value_name="Mean Absolute SHAP Value")
    comparison_df.rename(columns={"index": "Feature"}, inplace=True)

    # Plot using seaborn to match SHAP style
    colors = [color.red_rgb, color.blue_rgb]

    plt.figure(figsize=(10, 6))
    sns.set(style="whitegrid")
    sns.barplot(x="Mean Absolute SHAP Value", y="Feature", hue="Cohort", data=comparison_df, palette=colors)

    # Add the exact SHAP value at the end of each bar
    for index, row in comparison_df.iterrows():
        y_position = index % len(features) + (-0.2 if row['Cohort'] == 'Women' else +0.2)
        plt.text(
            row['Mean Absolute SHAP Value'] + 0.003,  # Position the text slightly to the right of the bar
            y_position,
            f"+{row['Mean Absolute SHAP Value']:.2f}",  # Format the value
            color=colors[0] if row['Cohort'] == 'Women' else colors[1],  # Match text color with the bar
            ha="center",
            va="center"
        )

    plt.title("Comparison of Mean Absolute SHAP Values between Men and Women")
    plt.xlabel("mean(|SHAP value|)")
    plt.ylabel("")
    plt.legend(title="", loc='upper right')
    plt.show()

def plot_sex_fairness_comparision_with_std(df, shap_values, X_test, features):
    import shap.plots.colors as color
    
    # Manually create cohorts based on the original 'Sex' column
    sex_cohort = [sex for sex in df.loc[X_test.index, 'Sex']]

    # Create a DataFrame for SHAP values and corresponding cohorts
    shap_df = pd.DataFrame(shap_values.values, columns=features)
    shap_df['Sex'] = sex_cohort.copy()

    # Compute mean and std of absolute SHAP values for each feature, separated by cohort
    shap_abs_women = shap_df[shap_df['Sex'] == 0].drop('Sex', axis=1).abs()
    mean_abs_shap_values_women = shap_abs_women.mean()
    std_abs_shap_values_women = shap_abs_women.std()

    shap_abs_men = shap_df[shap_df['Sex'] == 1].drop('Sex', axis=1).abs()
    mean_abs_shap_values_men = shap_abs_men.mean()
    std_abs_shap_values_men = shap_abs_men.std()

    # Create a combined DataFrame for plotting, including the std
    women_df = pd.DataFrame({
        'Feature': mean_abs_shap_values_women.index,
        'Cohort': 'Women',
        'Mean': mean_abs_shap_values_women.values,
        'Std': std_abs_shap_values_women.values
    })

    men_df = pd.DataFrame({
        'Feature': mean_abs_shap_values_men.index,
        'Cohort': 'Men',
        'Mean': mean_abs_shap_values_men.values,
        'Std': std_abs_shap_values_men.values
    })

    comparison_df = pd.concat([women_df, men_df], ignore_index=True)

    # Plot using seaborn to match SHAP style
    colors = [color.red_rgb, color.blue_rgb]

    plt.figure(figsize=(10, 6))
    sns.set(style="whitegrid")
    ax = sns.barplot(
        x="Mean",
        y="Feature",
        hue="Cohort",
        data=comparison_df,
        palette=colors,
        ci=None
    )

    # Add the error bars representing the std
    for patch, row in zip(ax.patches, comparison_df.itertuples()):
        x = patch.get_width()
        y = patch.get_y() + patch.get_height() / 2

        std = getattr(row, 'Std')
        cohort = getattr(row, 'Cohort')
        mean_val = getattr(row, 'Mean')

        # Plot the error bar
        ax.errorbar(
            x,
            y,
            xerr=std,
            fmt='none',
            ecolor='black',
            capsize=3
        )

        # Add the exact SHAP value at the end of each bar
        color_index = 0 if cohort == 'Women' else 1
        text_color = colors[color_index]
        ax.text(
            x + std + 0.005,
            y,
            f"+{mean_val:.2f}",
            color=text_color,
            ha="left",
            va="center"
        )

    plt.title("Comparison of Mean Absolute SHAP Values between Men and Women")
    plt.xlabel("mean(|SHAP value|)")
    plt.ylabel("")
    plt.legend(title="", loc='upper right')
    plt.show()

def plot_capital_loss_distribution(df):
    capital_loss_counts = df['Capital Loss'].value_counts().sort_index()
    #capital_loss_mean = df['Capital Loss'].mean()
    
    # Create the plot
    plt.figure(figsize=(9, 6))
    ax = sns.countplot(data=df, x='Capital Loss', order=capital_loss_counts.index)
    ax.set_title("Distribution of the values of the Capital Loss feature")
    ax.set_xlabel("Capital Loss in USD")
    ax.set_ylabel("Count of samples")
    #ax.set_yscale('log')

    # Get sorted unique 'Capital Loss' values
    capital_loss_values = capital_loss_counts.index.tolist()

    # Decide how many x-ticks you want to display
    num_ticks = 20  # Adjust as needed

    # Generate positions for the x-ticks
    tick_positions = np.linspace(0, len(capital_loss_values) - 1, num=num_ticks, dtype=int)

    # Ensure the maximum value is included in the tick labels
    if capital_loss_values[-1] not in [capital_loss_values[i] for i in tick_positions]:
        tick_positions = np.append(tick_positions, len(capital_loss_values) - 1)

    # Get the labels for these positions
    tick_labels = [capital_loss_values[pos] for pos in tick_positions]

    # Set the x-ticks and labels
    ax.set_xticks(tick_positions)
    ax.set_xticklabels(tick_labels, rotation=45)

    plt.tight_layout()
    plt.show()


def print_feedback(number, valid_solution):
    denomination = {1: "first", 2: "second", 3: "third"}
    if valid_solution:
        print(f"The {denomination[number]} solution is correct")
    else:
        print(f"The {denomination[number]} solution is incorrect")


def test_solution(solution, number, customer_id, mlp_model, X_test):
    feature_name, new_value = list(solution.items())[0]
    customer = X_test.iloc[[customer_id]].copy()
    customer[feature_name] = new_value
    y_pred = mlp_model.predict(customer)
    print_feedback(number, y_pred)


def test_simulation(
    first_solution, second_solution, third_solution, customer_id, mlp_model, X_test
):
    test_solution(first_solution, 1, customer_id, mlp_model, X_test)
    test_solution(second_solution, 2, customer_id, mlp_model, X_test)
    test_solution(third_solution, 3, customer_id, mlp_model, X_test)
