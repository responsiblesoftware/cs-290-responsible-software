from otter.test_files import test_case

OK_FORMAT = False

name = "accuracy"
points = None

@test_case(points=None, hidden=False)
def tests_accuracy(lr_acc, mlp_acc):
    import math
    assert math.isclose(lr_acc, 0.7941, abs_tol=1e-3)
    assert math.isclose(mlp_acc, 0.8394, abs_tol=1e-3)
