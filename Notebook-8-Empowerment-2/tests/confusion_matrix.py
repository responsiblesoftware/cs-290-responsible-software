from otter.test_files import test_case

OK_FORMAT = False

name = "confusion_matrix"
points = None

@test_case(points=None, hidden=False)
def test_confusion_matrix(cm_lr, cm_mlp, np):
	assert (cm_lr == np.array([[4157,  246], [  955, 476]])).all()
	assert (cm_mlp == np.array([[4155,  248], [  689, 742]])).all()

