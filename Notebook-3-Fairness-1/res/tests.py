import pandas as pd

inputs = {
    "foo": {"col1": [2, 3], "col2": ['cats', 'dogs'], "patrick": None},
    "normalization" : {"df": pd.DataFrame({"SAT": [1300,1400,900], "GPA": [3.61,3.67,4], "Interest": [10,0,5]})},
    "is_outlier" : {"df": pd.DataFrame({"SAT_norm": [8.12,8.75,5.62,8.50], "GPA_norm": [7.22,7.34,8,6.38],
                                        "Interest": [10,0,5,0]})}
}
# inputs = dictionary that maps arguments' name to their input

expecteds = {
    "foo": (pd.DataFrame(data=[[2, 3], ['cats', 'dogs']])),
    "normalization" : (pd.DataFrame({"SAT": [1300,1400,900], "GPA": [3.61,3.67,4], "Interest": [10,0,5], "SAT_norm": [8.125,8.750,5.625], "GPA_norm": [7.22,7.34,8]})),
    "is_outlier" : (pd.DataFrame({"SAT_norm": [8.12,8.75,5.62,8.50], "GPA_norm": [7.22,7.34,8,6.38],
                                "Interest": [10,0,5,0], "is_outlier": [False,True,True,True]}))
}
# output in a tuple

common_mistakes = {
    "normalization" : [
        "- The coefficients are not correct.",
        "- The names of the new columns are not respected."],
    "is_outlier" : [
        "- The boolean operators are not correct.",
        "- You did not take the normalized columns of SAT and GPA.",
        "- The name of the new column is not respected."]
}

def test(fct):
    actual = fct(**inputs[fct.__name__])
    expected = expecteds[fct.__name__]
    if actual.__repr__() == expected.__repr__():
        #print("🆗 Tests passed ! (〃￣︶￣)人(￣︶￣〃)")
        print("✅ You passed the test!")
    else:
        #print("❌ Tests failed. ┌(´_ゝ`)┐")
        print("❌ There are some issues with your implementation")
        print("--------------------------------------")
        print(f"Expected:")
        display(expected)
        print(f"Actual:")
        display(actual)
        print("--------------------------------------")
        print("💡Here are possible mistakes that you may have done:")
        error_messages = "\n".join(common_mistakes[fct.__name__])
        print(error_messages)
        print("--------------------------------------")
