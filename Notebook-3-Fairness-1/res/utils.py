# Support file with useful functions
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
import seaborn as sns
import pandas as pd

import warnings
warnings.simplefilter("ignore")

rand_seed = 1234
np.random.seed(rand_seed)

def load_dataset():
    df = pd.read_csv("./dataset/admission_algorithms_dataset.csv")
    #df.set_index('Student', inplace=True)
    
    for col in list(df.columns):
        if col != "Student":
            df[col] = df[col].astype(float)
    
    return df


def test_result(function_name, df):
    if (function_name.__name__ == 'calculate_student_score'):
        if (list(df['Student score'].iloc[:3].values) == [7.73, 7.36, 5.79]):
            print(f"✅ You passed the test !")
        else : 
            print(f"❌ There are some issues with your implementation")
            print("--------------------------------------")
            print("💡Here are possible mistakes that you may have done:")
            print("- The order of the coefficients is incorrect.\n- You did not take the normalized columns of SAT and GPA.\n- The name of the new column is not respected.\n- The values have not been rounded to 2 decimal places.")
            print("--------------------------------------")
            
    if (function_name.__name__ == 'apply_selection_base'):
        if (np.sum(df["selected_base"] == True) == 159):
            print(f"✅ You passed the test !")
        else : 
            print(f"❌ There are some issues with your implementation")
            print("--------------------------------------")
            print("💡Here are possible mistakes that you may have done:")
            print("- The condition is not correct.\n- You did not take the correct column of the dataframe.")
            print("--------------------------------------")
            
    if (function_name.__name__ == 'apply_selection_improved'):
        if (np.sum(df["selected_improved"] == True) == 171):
            print("✅ You passed the test !")
        else : 
            print("❌ There are some issues with your implementation")
            print("--------------------------------------")
            print("💡Here are possible mistakes that you may have done:")
            print("- The conditions are not correct.\n- The booleans operators are not correct.\n- You forgot the parenthesis.\n- You did not take the correct columns of the dataframe in the conditions.")
            print("--------------------------------------")
            
def choice_visualization(visu_type):
    
    df_initial = load_dataset()
    df_initial['SAT_norm'] = df_initial['SAT']/160
    df_initial['GPA_norm'] = 2*df_initial['GPA']
    df_initial['High School Quality'].replace({0: "low", 1 : "low", 2: "low", 3: "mid", 4 : "mid", 5 : "mid", 6 : "mid", 7 : "mid", 8: "high", 9: "high", 10: "high"}, inplace=True)

    
    if visu_type == 'boxplot':
        fig, axs = plt.subplots(1, 3, figsize=(15,4))
        
        df_melted=df_initial.melt(id_vars=['High School Quality'], value_vars=['SAT_norm'], var_name='Variable', value_name='Value')
        sns.boxplot(data = df_melted, y = 'Variable', x='Value', hue = 'High School Quality', hue_order = ['high', 'mid', 'low'], ax = axs[0], showmeans=True, meanprops={"marker":"x", "markerfacecolor":"black", "markeredgecolor":"black"})
        axs[0].set_yticks([])
        axs[0].set_ylabel("")
        axs[0].set_xlabel("SAT_norm")

        df_melted=df_initial.melt(id_vars=['High School Quality'], value_vars=['GPA_norm'], var_name='Variable', value_name='Value')
        sns.boxplot(data = df_melted, y = 'Variable', x='Value', hue = 'High School Quality', hue_order = ['high', 'mid', 'low'], ax = axs[1], showmeans=True, meanprops={"marker":"x", "markerfacecolor":"black", "markeredgecolor":"black"})
        axs[1].set_yticks([])
        axs[1].set_ylabel("")
        axs[1].set_xlabel("GPA_norm")

        df_melted=df_initial.melt(id_vars=['High School Quality'], value_vars=['Interest'], var_name='Variable', value_name='Value')
        sns.boxplot(data = df_melted, y = 'Variable', x='Value', hue = 'High School Quality', hue_order = ['high', 'mid', 'low'], ax = axs[2], showmeans=True, meanprops={"marker":"x", "markerfacecolor":"black", "markeredgecolor":"black"})
        axs[2].set_yticks([])
        axs[2].set_ylabel("")
        axs[2].set_xlabel("Interest")


        plt.tight_layout()
        
        for i in range(3):
            axs[i].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left')
            axs[i].set_title("Boxplot")
    
    elif visu_type == 'histogram':
        fig, axs = plt.subplots(1,3, figsize = (15,5))
        
        sns.histplot(data = df_initial, kde = True, x = 'SAT_norm', hue = 'High School Quality', hue_order = ['high', 'mid', 'low'], ax = axs[0])
        sns.histplot(data = df_initial, kde = True, x = 'GPA_norm', hue = 'High School Quality', hue_order = ['high', 'mid', 'low'], ax = axs[1])
        sns.histplot(data = df_initial, kde = True, x = 'Interest', hue = 'High School Quality', hue_order = ['high', 'mid', 'low'], ax = axs[2])
        plt.tight_layout()
        #fig.legend()
        
        for i in range(3):
            axs[i].set_title("Histogram")
        
    else:
        print("You should choose between boxplot and histogram.")


def update_lists_passing_students(df):
    '''
    This function updates the lists of students who passed the selection based on the 2 versions of the selection algorithm
    
    trick 1: since the columns contain booleans, we just need to sum the values to get the number of students with a True/1
    trick 2: we "turn" the resulting dataframe using .T to get a more readable plot
    '''
    selection_summary = df.groupby('High School Quality')[['selected_base', 'selected_improved']].agg({"selected_base" : "sum", "selected_improved" : "sum"}).reindex(['high', 'mid', 'low']).T
    
    # computing the corresponding percentages
    def percent(x):
        return x.sum()/x.count()*100
    selection_summary_perct = df.groupby('High School Quality')[['selected_base', 'selected_improved']].agg({"selected_base" : percent, "selected_improved" : percent}).reindex(['high', 'mid', 'low']).T
    
    # renaming the columns to have pretty labels for plotting
    labels = {"selected_base": "Passed selection base", "selected_improved": "Passed selection improved"}
    selection_summary.rename(index=labels, inplace=True)
    selection_summary_perct.rename(index=labels, inplace=True)

    return selection_summary, selection_summary_perct

def plot_selection(selection_summary, selection_summary_perct):
    variables = ['selected_base', 'selected_improved']
    fig, axs = plt.subplots(1,2, figsize = (12,4))
    
    selection_summary.plot(kind='barh', ax = axs[0])
    axs[0].set_title("Number of students passing the selections per HSQ")
    axs[0].set_xlabel("Number of students")
    
    selection_summary_perct.plot(kind='barh', ax = axs[1])
    axs[1].set_title("Percentage of students passing the selections per HSQ")
    axs[1].set_xlabel("Percentage of students")
    
    for i in range(len(variables)):
        for c in axs[i].containers:
            labels = [f'{val:.1f}' for val in c.datavalues]
            axs[i].bar_label(c, labels = labels, label_type='center')
            
    plt.tight_layout()
    
def load_berkeley_dataset():
    berkeley = pd.read_csv("./dataset/berkeley.csv")
    berkeley.drop(columns = 'Year', inplace=True)
    return berkeley

def plot_distributions(df):
    df['High School Quality'].replace({0: "low", 1 : "low", 2: "low", 
                                    3: "mid", 4 : "mid", 5 : "mid", 6 : "mid", 7 : "mid",
                                    8: "high", 9: "high", 10: "high"}, inplace=True)

    fig, axs = plt.subplots(1,3, figsize = (15,5))

    sns.histplot(data = df, kde = True, x = 'Interest', hue = 'High School Quality', ax = axs[0])
    axs[0].set_title("Interest distrib.")

    sns.histplot(data = df, kde = True, x = 'GPA', hue = 'High School Quality', ax = axs[1])
    axs[1].set_title("GPA distrib.")

    sns.histplot(data = df, kde = True, x = 'SAT', hue = 'High School Quality', ax = axs[2])
    axs[2].set_title("SAT distrib.")
    plt.tight_layout()
    
def plot_admissions_proportions():
    berkeley = pd.read_csv("./dataset/berkeley.csv")
    
    # Calculate proportions for Male and Female applicants by Major and Admission
    proportion_M = berkeley[berkeley['Gender'] == 'M'].groupby(['Major', 'Admission']).size().unstack().apply(lambda x: x / x.sum(), axis=1)
    proportion_F = berkeley[berkeley['Gender'] == 'F'].groupby(['Major', 'Admission']).size().unstack().apply(lambda x: x / x.sum(), axis=1)
    proportion_M['Rejected'] = -proportion_M['Rejected']
    proportion_F['Rejected'] = -proportion_F['Rejected']

    # Combine Male and Female proportions for each major
    combined_proportions = pd.concat([proportion_M, proportion_F], axis=1)
    combined_proportions.columns = pd.MultiIndex.from_product([['Male', 'Female'], ['Accepted', 'Rejected']])

    # Predefined color names in seaborn
    palette_colors = ["limegreen", "firebrick", "lightgreen", "lightcoral"]
    
    ax = combined_proportions.plot(kind='barh', figsize=(15, 6), color = palette_colors)
    plt.vlines(x=0, ymin=-1, ymax=20, colors='black', linestyles='-')
    plt.title("Admission Proportions by Major for Male and Female Applicants")
    plt.xlabel("Major")
    plt.ylabel("Proportion")
    plt.legend(title='Gender - Admission', loc='center left', bbox_to_anchor=(1, 0.5))
    plt.tick_params(axis='x',labelrotation = 0)
    
    # Access the bar containers and apply hatches to Female bars
    for i, container in enumerate(ax.containers):
        if i == 2 or i == 3:
            for bar in container:
                bar.set_hatch('///')  # Apply hatches to Female bars
    
    # Create custom legend handles for hatches
    legend_elements = [Patch(facecolor='white', edgecolor='black', hatch='///', label='Female - Rejected')]
    # Get the current handles and labels from the plot
    handles, labels = ax.get_legend_handles_labels()
    # Add the custom legend handles and labels
    handles.extend(legend_elements)

    # Display the legend with updated handles and labels
    plt.legend(handles=handles, labels=labels, title='Gender - Admission', loc='center left', bbox_to_anchor=(1, 0.5))
    plt.show()