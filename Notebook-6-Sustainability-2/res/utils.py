import pandas as pd 
import pydoc
# import impact_helpers
import re
import inspect
import importlib
from IPython.display import Markdown, display

from matplotlib import pyplot as plt


def print_emissions_translation(emissions):
    print(
        "{:,} kg of CO₂eq are equivalent to:\n🚙 driving an average passenger car for {:,} km,\n🛫 taking an international flight for {:,} km or\n🚂 travelling by train for {:,} km.\n".format(
            emissions.at[0, "CO2_kg"],
            emissions.at[0, "car_km"],
            emissions.at[0, "plane_km"],
            emissions.at[0, "rail_km"],
        ).replace(',', ' ')
    )

def translate_emissions(CO2_kg):
    co2_kg_to_km_car = round(CO2_kg / 0.1639)  # SOLUTION
    co2_kg_to_km_plane = round(CO2_kg / 0.18592)  # SOLUTION
    co2_kg_to_km_rail = round(CO2_kg / 0.00446)  # SOLUTION

    df = pd.DataFrame(
        {
            "CO2_kg": [round(CO2_kg)],
            "car_km": [co2_kg_to_km_car],
            "plane_km": [co2_kg_to_km_plane],
            "rail_km": [co2_kg_to_km_rail],
        }
    )
    print_emissions_translation(df)
    
def plot_PCF_mass_Dell(pcf_dell):
    # Create the figure and the first axis
    fig, ax1 = plt.subplots(figsize=(12, 6))
    
    bars = ax1.bar(pcf_dell['Parts'].apply(lambda x: x[:20] + ("..." if len(x) > 20 else "")), 
                   pcf_dell['Emissions (kg of CO2 eq.)'], 
                   color='skyblue', label='Emissions')
    
    # Customize the first axis
    ax1.set_xlabel('Components', fontsize=12)
    ax1.set_ylabel('GWP (kg CO2 eq.)', fontsize=12)
    ax1.tick_params(axis='x', rotation=45)
    ax1.tick_params(axis='y', colors='blue')
    ax1.grid(axis='y', linestyle='--', alpha=0.7)
    ax1.set_yscale('log')
    
    # Add value labels on top of each bar
    for bar in bars:
        height = bar.get_height()
        ax1.text(bar.get_x() + bar.get_width()/2., height,
                 f'{height:.2f}',
                 ha='center', va='bottom', rotation=0)
    
    # Create the second axis
    ax2 = ax1.twinx()
    ax2.plot(pcf_dell['Parts'].apply(lambda x: x[:20] + ("..." if len(x) > 20 else "")), 
             pcf_dell['Mass (kg)'], 
             color='red', marker='o', linestyle='-', label='Mass')
    
    # Customize the second axis
    ax2.set_ylabel('Mass (kg)', fontsize=12)
    ax2.tick_params(axis='y', colors='red')
    
    # Add legends
    fig.legend(loc='upper right', bbox_to_anchor=(1,1), bbox_transform=ax1.transAxes)
    
    # Set the title
    ax1.set_title('Comparative Analysis of Component Emissions and Mass in Product Carbon Footprint (PCF) for Dell Parts', fontsize=14)
    
    # Adjust layout and display the plot
    fig.tight_layout()
    plt.show()
    
def display_function_docs(module_name):
    docs = {}
    module = importlib.import_module(module_name)
    function_names = [fn for fn, _ in inspect.getmembers(module, inspect.isfunction)]
    for fn in function_names:
        doc = pydoc.render_doc(getattr(module, fn), renderer=pydoc.plaintext)
        doc = re.sub(r".*=\s", "", doc)
        doc = [d for d in doc.split("\n") if d != ""][1:]
        doc = "\n".join(doc)
        doc = re.sub(fn, f"<b style='font-weight:bold;'>{fn}</b>", doc)
        doc = re.sub(rf"(^.*\b{re.escape(fn)}\b.*$)", r"<h5 style='text-decoration: underline; margin-bottom: 5px; margin-top: 0;'>\1</h5>", doc, flags=re.MULTILINE)

        # Style the 'Args' and 'Returns' sections with colors
        doc = re.sub(r"(Args:)", 
                r"<div style='background-color: #e8f5e9; padding: 5px; border-radius: 6px; margin: 2px 0;'><b style='color: #2e7d32;'>\1</b>", doc)  # Header for Args with green background
        doc = re.sub(r"(Returns:)", 
            r"</div><div style='background-color: #e3f2fd; padding: 5px; border-radius: 6px; margin: 2px 0;'><b style='color: #1565c0;'>\1</b>", doc)  # Header for Returns with blue background
        doc = re.sub(r"(Raises:)",
            r"</div><div style='background-color: #ffcccb; padding: 5px; border-radius: 6px; margin: 2px 0;'><b style='color: #9a0e01;'>\1</b>", doc) # Header for Raises with red background
        
        # Highlight argument names in green and orange, and argument types in flashy bold orange
        doc = re.sub(r"(\b\w+\b)( \([^)]+\):)", r"<span style='color: #43a047; font-weight: bold;'>\1</span><span style='color: #f57f17;'>\2</span>", doc)

        # Highlight return descriptions in blue and types in bold violet
        doc = re.sub(r"(Returns:</b>\n\s*)([^:]+)(:)", 
                r"\1<span style='color: #2979ff; font-weight: bold;'>\2</span><span style='color: #2979ff; font-weight: bold;'>\3</span>", doc)
        
        # Highlight raises descriptions in red and types in bold dark purple
        doc = re.sub(r"(Raises:</b>\n\s*)([^:]+)(:)",
            r"\1<span style='color: #e7472c; font-weight: bold;'>\2</span><span style='color: #e7472c; font-weight: bold;'>\3</span>", doc)
       
        doc = doc.rstrip()
        if doc.count('<div') > doc.count('</div'):
            doc += '</div>'
        doc = (
            f"<pre style='color: #424242; font-size: 12px; white-space: pre-wrap; margin: 0; padding: 0;'>{doc}</pre>"
        )
        docs[fn] = doc

    return docs

impact_helpers_docs = display_function_docs("res.impact_helpers")

def get_3_1_guidelines(entry1, entry2):
    table_html = (
        f"<table style=\"width:100%; border-collapse: separate; border-spacing: 10px;\">"
        f"<tr>"
        f"<td style=\"width:49.5%; vertical-align: top; background-color: #f5f5f5; border-radius: 8px; padding: 15px;\">"
        f"{entry1}"
        f"</td>"
        f"<td style=\"width:49.5%; vertical-align: top; background-color: #f5f5f5; border-radius: 8px; padding: 15px;\">"
        f"{entry2}"
        f"</td>"
        f"</tr>"
        f"</table>"
    )
    display(Markdown(table_html))

def get_3_1_guidelines_decorator():
    get_3_1_guidelines(impact_helpers_docs['round_impact'], impact_helpers_docs['validate_indicator'])
    
def get_3_1_guidelines_impact():
    get_3_1_guidelines(impact_helpers_docs['base_impact'], impact_helpers_docs['die_impact'])

