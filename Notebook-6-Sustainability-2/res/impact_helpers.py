def validate_indicator(func):
    """
    A decorator that check if the 'indicator' argument passed to the wrapped function is valid. To be valid, the indicator must be one of the following: "GWP", "PE", or "ADP". If the indicator is invalid, it raises a ValueError.

    Args:
        func (function): The function to be wrapped. The wrapped function must have an 'indicator' argument, ideally as the second argument.
        
    Returns:
        function: The wrapper function with added validation for the 'indicator' argument.
        
    Raises:
        ValueError: If 'indicator' is not one of the valid options ("GWP", "PE", or "ADP").
    """
    def wrapper(*args, **kwargs):
        indicator = kwargs.get('indicator') or args[1]
        if indicator not in ["GWP", "PE", "ADP"]:
            raise ValueError(f"Indicator {indicator} not valid.")
        return func(*args, **kwargs)
    return wrapper

def round_impact(func):
    """
    A decorator that rounds the result of the wrapped function to two decimal places for the 'PE' and 'GWP' indicators, and to five decimal places for the 'ADP' indicator.

    Args:
        func (function): The function to be wrapped. The wrapped function should return a numerical value. The wrapped function should also have an 'indicator' argument, ideally as the second argument.

    Returns:
        function: The wrapper function that rounds the output.
    """
    def wrapper(*args, **kwargs):
        return round(func(*args, **kwargs), {"PE": 2, "GWP": 2, "ADP": 5}[kwargs.get('indicator') or args[1]])
    return wrapper

def get_impact(impact_data, component, part, indicator):
    """
    Retrieves the environmental impact for a specific component part and indicator from the provided data.

    Args:
        impact_data (pd.DataFrame): The DataFrame containing impact data.
        component (str): The name of the component (e.g., "CPU").
        part (str): The part of the component (e.g., "Die", "Base").
        indicator (str): The environmental indicator to use (e.g., "GWP", "PE", "ADP").

    Returns:
        float: The impact value for the specified component part and indicator.
    """
    component_name = " ".join([component.upper(), part])
    return impact_data.loc[component_name, indicator]

def die_impact(impact_data, component, indicator):
    """
    Retrieves the environmental impact of the 'Die' part of a specified component.

    Args:
        impact_data (pd.DataFrame): The DataFrame containing impact data.
        component (str): The name of the component (e.g., "CPU").
        indicator (str): The environmental indicator to use (e.g., "GWP", "PE", "ADP").

    Returns:
        float: The impact value for the 'Die' part of the component.
    """
    return get_impact(impact_data, component, "Die", indicator)

def base_impact(impact_data, component, indicator):
    """
    Retrieves the environmental impact of the 'Base' part of a specified component.

    Args:
        impact_data (pd.DataFrame): The DataFrame containing impact data.
        component (str): The name of the component (e.g., "CPU").
        indicator (str): The environmental indicator to use (e.g., "GWP", "PE", "ADP").

    Returns:
        float: The impact value for the 'Base' part of the component.
    """
    return get_impact(impact_data, component, "Base", indicator)