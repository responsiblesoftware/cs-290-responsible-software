from otter.test_files import test_case
import numpy as np
from math import isclose

import pandas as pd 

OK_FORMAT = False

name = "dell_pcf_analysis_q2"
points = None
hidden = False

@test_case(name = "Server GWP (Dell)")
def test_r740_server_gwp_dell(total_gwp_r740_dell):
    assert int(total_gwp_r740_dell) == 4268, "The total emissions computed is incorrect."
    
