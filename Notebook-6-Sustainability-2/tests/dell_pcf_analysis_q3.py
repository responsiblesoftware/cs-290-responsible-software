from otter.test_files import test_case
import numpy as np
from math import isclose

import pandas as pd 

OK_FORMAT = False

name = "dell_pcf_analysis_q3"
points = None
hidden = False

@test_case(name = "DataFrame is Sorted")
def test_sorted(pcf_dell):
    assert pcf_dell["Mass (kg)"].is_monotonic_decreasing, "The DataFrame is not sorted."
