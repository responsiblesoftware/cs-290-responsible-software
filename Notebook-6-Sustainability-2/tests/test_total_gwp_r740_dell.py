from otter.test_files import test_case
import pandas as pd
import math

OK_FORMAT = False

name = "Total GWP (R740) - Dell"
points = None
hidden = False

@test_case(name="Total GWP (R740) - Dell")
def test_total_gwp_r740(total_gwp_r740_dell):
    assert math.isclose(total_gwp_r740_dell, 889, abs_tol=1), "The total GWP for the R740 server (Dell) is incorrect."

