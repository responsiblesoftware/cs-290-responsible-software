from otter.test_files import test_case
import pandas as pd
import math

OK_FORMAT = False

name = "Total Impact"
points = None
hidden = False

mock_impact_data = {
        "Component": [
            "CPU Base", "CPU Die", "RAM Base", "RAM Die", 
            "SSD Base", "SSD Die", "HDD", "Motherboard", 
            "Rack Server", "Blade Enclosure", "Blade Server", 
            "Server Assembly", "Power Supply Unit"
        ],
        "ADP": [
            1.5E-02, 6.90E-07, 2.40E-03, 3.70E-05, 
            4.50E-04, 6.40E-05, 1.90E-04, 5.10E-03, 
            3.00E-02, 4.90E-01, 7.00E-04, 1.50E-06, 7.90E-03
        ],
        "GWP": [
            11.0, 3.20, 5.40, 3.40, 
            7.10, 5.20, 21.00, 66.90, 
            160.10, 870.10, 41.20, 8.20, 21.70
        ],
        "PE": [
            140, 28.00, 83.00, 31.00, 
            76.00, 25.00, 216.30, 823.40, 
            2352.00, 13830.60, 472.30, 46.30, 363.00
        ]
    }
mock_impact_data = pd.DataFrame(mock_impact_data)
mock_impact_data.set_index("Component", inplace=True)

@test_case(points=None, hidden=False)
def test_calculate_total_impact(calculate_total_impact):
    # Test with GWP indicator
    result_gwp = calculate_total_impact(mock_impact_data, "GWP", 
                                     cpu_units=2, cpu_cores=8, 
                                     ram_units=4, ram_size=16, 
                                     ssd_units=2, ssd_size=500, 
                                     hdd_units=1, 
                                     psu_units=1, psu_weight=0.5, 
                                     case_type="Rack")
    
    expected_gwp = 564.87  # Corrected expected result for GWP
    assert math.isclose(result_gwp, expected_gwp, rel_tol=1.E-3), f"Total GWP is incorrect: {result_gwp}, expected {expected_gwp}"

    # Test with PE indicator
    result_pe = calculate_total_impact(mock_impact_data, "PE", 
                                    cpu_units=2, cpu_cores=8, 
                                    ram_units=4, ram_size=16, 
                                    ssd_units=2, ssd_size=500, 
                                    hdd_units=1, 
                                    psu_units=1, psu_weight=0.5, 
                                    case_type="Rack")
    expected_pe = 6123.21
    assert math.isclose(result_pe, expected_pe, rel_tol=1.E-3), f"Total PE is incorrect: {result_pe}, expected {expected_pe}"
    
    # Test with ADP indicator
    result_adp = calculate_total_impact(mock_impact_data, "ADP", 
                                    cpu_units=2, cpu_cores=8, 
                                    ram_units=4, ram_size=16, 
                                    ssd_units=2, ssd_size=500, 
                                    hdd_units=1, 
                                    psu_units=1, psu_weight=0.5, 
                                    case_type="Rack")
    expected_adp = 0.08232
    assert math.isclose(result_adp, expected_adp, rel_tol=1.E-6), f"Total ADP is incorrect: {result_adp}, expected {expected_adp}"
    
    
    # Test invalid indicator
    try:
        calculate_total_impact(mock_impact_data, "INVALID", 
                               cpu_units=2, cpu_cores=8, 
                               ram_units=4, ram_size=16, 
                               ssd_units=2, ssd_size=500, 
                               hdd_units=1, 
                               psu_units=1, psu_weight=0.5, 
                               case_type="Rack")
        assert False, "Invalid indicator should raise a ValueError"
    except ValueError as e:
        assert str(e) == "Indicator INVALID not valid.", f"Unexpected error message: {str(e)}"

