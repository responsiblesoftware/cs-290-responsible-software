from otter.test_files import test_case
import pandas as pd
import math
OK_FORMAT = False

name = "Impact RAM Formula Test"
points = None
hidden = False

mock_impact_data = {
        "Component": [
            "CPU Base", "CPU Die", "RAM Base", "RAM Die", 
            "SSD Base", "SSD Die", "HDD", "Motherboard", 
            "Rack Server", "Blade Enclosure", "Blade Server", 
            "Server Assembly", "Power Supply Unit"
        ],
        "ADP": [
            1.5E-02, 6.90E-07, 2.40E-03, 3.70E-05, 
            4.50E-04, 6.40E-05, 1.90E-04, 5.10E-03, 
            3.00E-02, 4.90E-01, 7.00E-04, 1.50E-06, 7.90E-03
        ],
        "GWP": [
            11.0, 3.20, 5.40, 3.40, 
            7.10, 5.20, 21.00, 66.90, 
            160.10, 870.10, 41.20, 8.20, 21.70
        ],
        "PE": [
            140, 28.00, 83.00, 31.00, 
            76.00, 25.00, 216.30, 823.40, 
            2352.00, 13830.60, 472.30, 46.30, 363.00
        ]
    }
MOCK_IMPACT_DATA = pd.DataFrame(mock_impact_data)
MOCK_IMPACT_DATA.set_index("Component", inplace=True)

@test_case(name="Calculate RAM Impact")
def test_calculate_ram_impact(calculate_ram_impact):
    # Test with GWP indicator
    result_gwp = calculate_ram_impact(MOCK_IMPACT_DATA, "GWP", 2, 8, 1.79)
    expected_gwp = 41.19 # Corrected expected result for GWP
    assert math.isclose(result_gwp, expected_gwp, abs_tol=1.E-2), f"GWP impact is incorrect: {result_gwp}, expected {expected_gwp}"

    # Test with PE indicator
    result_pe = calculate_ram_impact(MOCK_IMPACT_DATA, "PE", 2, 8, 1.79)
    expected_pe = 443.09 # Corrected expected result for PE
    assert math.isclose(result_pe, expected_pe, abs_tol=1.E-2), f"PE impact is incorrect: {result_pe}, expected {expected_pe}"

    # Test with ADP indicator
    result_adp = calculate_ram_impact(MOCK_IMPACT_DATA, "ADP", 2, 8, 1.79)
    expected_adp = 0.00513 # Corrected expected result for ADP
    assert math.isclose(result_adp, expected_adp, abs_tol=1.E-5), f"ADP impact is incorrect: {result_adp}, expected {expected_adp}"

    # Test invalid indicator
    try:
        calculate_ram_impact(MOCK_IMPACT_DATA, "INVALID", 2, 8, 1.79)
        assert False, "Invalid indicator should raise a ValueError"
    except ValueError as e:
        assert str(e) == "Indicator INVALID not valid.", f"Unexpected error message: {str(e)}"
