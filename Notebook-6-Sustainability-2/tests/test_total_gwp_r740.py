from otter.test_files import test_case
import pandas as pd
import math

OK_FORMAT = False

name = "Total GWP (R740)"
points = None
hidden = False

@test_case(name="Total GWP (R740)")
def test_total_gwp_r740(total_gwp_r740):
    assert math.isclose(total_gwp_r740, 897.39, abs_tol=1e-2), "The total GWP for the R740 server is incorrect."

