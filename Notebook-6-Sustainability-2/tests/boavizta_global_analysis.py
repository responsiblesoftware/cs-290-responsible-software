from otter.test_files import test_case
import pandas as pd
import math
from functools import partial

OK_FORMAT = False

name = "Tests"
points = None
hidden = False

isclose = partial(math.isclose, rel_tol=1e-6)

@test_case(name = "Selected Columns")
def test_selected_columns(emissions_breakdown):
    # Expected DataFrame after filtering and selecting columns
    expected_columns = {"gwp_manufacturing_ratio", "gwp_transport_ratio", "gwp_use_ratio", "gwp_eol_ratio"}
    # Check if the student's DataFrame matches the expected DataFrame 
    students_columns = set(emissions_breakdown.columns)
    assert students_columns == expected_columns, "The selected columns do not match the expected columns."

@test_case(name="Mean Ratios")
def test_mean_ratios(mean_ratios):
    # Expected mean values for each column
    assert isinstance(mean_ratios, pd.Series), "The mean_ratios should be an instance of pd.Series."
    expected_values = {
        "mean_manufacturing": 0.64790858,
        "mean_transport": 0.04372279,
        "mean_use": 0.32079973,
        "mean_eol": 0.005416666
    }
    for k, v in mean_ratios.items():
        assert k in mean_ratios, f"{k} is not in the mean_ratios."
        assert isclose(expected_values[k], v), f"The mean value {v} for {k} is incorrect (Expected : {expected_values[k]}]."

@test_case(name="GWP Manufacturing")
def test_gwp_manufacturing(add_gwp_manufacturing):
    df = pd.DataFrame({
        "gwp_manufacturing_ratio": [0, 0.6, 0.7, 0.8, 0.9],
        "gwp_total": [1000, 200, 300, 0, 500]
    })
    x = add_gwp_manufacturing(df)
    assert x is None, "The function should not return anything."
    assert df["gwp_manufacturing"].equals(pd.Series([0., 120., 210., 0., 450.])), "The gwp_manufacturing column is incorrectly computed."



@test_case(name="Replace Missing GWP Manufacturing")
def test_replace_missing_gwp_manufacturing(replace_missing_gwp_manufacturing):
    df = pd.DataFrame({
        "gwp_manufacturing": [math.nan, 120, 210, math.nan, math.nan],
        "gwp_use_ratio": [0.3, 0.2, 0.1, 0.4, math.nan],
        "gwp_total": [1000, 200, 300, 0, 500]
    })
    x = replace_missing_gwp_manufacturing(df)
    assert x is None, "The function should not return anything."
    assert df["gwp_manufacturing"].equals(pd.Series([700., 120., 210., 0., math.nan])), "The gwp_manufacturing column is incorrectly computed."

@test_case(name="Mean GWP Manufacturing")
def test_mean_gwp_manufacturing(emissions_per_category_grouped):
    df = emissions_per_category_grouped.unstack()
    expected_values = {
        "Datacenter": 1473.150515,
        "Home": 36.192241,
        "Workplace": 266.694314,
    }
    for k, v in df.items():
        assert isclose(expected_values[k[1]], v), f"The mean value for {k} is incorrect."


@test_case(name="Mean Server Emissions")
def test_mean_server_emissions(mean_server_emissions):
    assert int(round(mean_server_emissions)) == 1703, "The mean value for server emissions is incorrect."
