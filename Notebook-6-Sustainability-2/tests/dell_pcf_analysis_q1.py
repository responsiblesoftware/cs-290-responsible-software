from otter.test_files import test_case
import numpy as np
from math import isclose

import pandas as pd 

OK_FORMAT = False

name = "dell_pcf_analysis_q1"
points = None
hidden = False

@test_case(name = "Server Configuration")
def test_r740_server_config(boavizta_data_server_manufacturing, number_cpu, memory, hard_drive):
    assert isclose(boavizta_data_server_manufacturing, 1313.28)
    assert int(number_cpu) == 2, "The number of CPUs is incorrect."
    assert int(memory) == 32, "The memory is incorrect."
    assert hard_drive == "x2 300GB 2.5in HDD  x1 1TB 2.5in HDD", "The hard drive config is incorrect."