from otter.test_files import test_case
import pandas as pd
import math
from functools import partial

OK_FORMAT = False

name = "Tests"
points = None
hidden = False

isclose = partial(math.isclose, rel_tol=1e-6)

@test_case(name = "Selected Columns")
def test_selected_columns(emissions_breakdown):
    # Expected DataFrame after filtering and selecting columns
    expected_columns = {"gwp_manufacturing_ratio", "gwp_transport_ratio", "gwp_use_ratio", "gwp_eol_ratio"}
    # Check if the student's DataFrame matches the expected DataFrame 
    students_columns = set(emissions_breakdown.columns)
    assert students_columns == expected_columns, "The selected columns do not match the expected columns."
