from otter.test_files import test_case
import numpy as np

OK_FORMAT = False

name = "Difference GWP Test (1)"
points = None
hidden = False

@test_case(name = "Calculate Difference GWP")
def test_calculate_difference_gwp(r740_gwp_comparison):
    expected_values = np.array([7.7,-0.3,62.92,-11.58])
    assert np.isclose(r740_gwp_comparison["Difference (%)"], expected_values , atol=1e-2).all(), "Difference GWP is incorrect"

    