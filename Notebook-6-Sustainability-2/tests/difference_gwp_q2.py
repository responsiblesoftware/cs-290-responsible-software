from otter.test_files import test_case
import numpy as np

OK_FORMAT = False

name = "Difference GWP Test (2)"
points = None
hidden = False

@test_case(name = "Calculate Difference GWP - Dell")
def test_calculate_difference_gwp_dell(r740_gwp_comparison):
    expected_values = np.array([5.29,59.96,7.20,29.81])
    assert np.isclose(r740_gwp_comparison["Dell LCA - GWP %"], expected_values , atol=1e-2).all(), "Difference GWP (Dell LCA) is incorrect"

@test_case(name = "Calculate Difference GWP - Our Analysis")
def test_calculate_difference_gwp_ours(r740_gwp_comparison):
    expected_values = np.array([4.83, 59.57, 2.64, 32.95])
    assert np.isclose(r740_gwp_comparison["Our analysis - GWP %"], expected_values , atol=1e-2).all(), "Difference GWP (Out Analysis) is incorrect"

    