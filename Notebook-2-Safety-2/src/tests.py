import pandas as pd

item = {"humour": -1.0, "sarcasm": 1.0,"offensive": 0.5, "motivational": -0.5, "overall_sentiment": 0.0, "author": "Alice"}
items = pd.DataFrame([item, item], index=["A", "A"])
user = pd.Series({"name": "Alice", "humour": 0.0, "sarcasm": 0.0, "offensive": 0.0, "motivational": 0.0, "overall_sentiment": 0.0}, name=0)
k = 2
item_to_promote = pd.Series({"humour": -1.0, "sarcasm": -1.0, "offensive": -1.0, "motivational": 1.0, "overall_sentiment": 1.0, "author": "Celebrations"}, name="image_0.png")

# "name of function": [
# {"arg1": value1, "arg2": value2}, # input of first test
# {"arg1": value3, "arg2": value4}, # input of second test
# ...
# ]
inputs = {
    "foo": [
        {"col1": [2, 3], "col2": ['cats', 'dogs'], "patrick": None},
        {"col1": [10, 51, 0], "col2": ["parrots", "rabbits", "wolfs"]},
    ],
    "selector_advertisement": [
        {"items": items, "user": user, "k": k}
    ]
}

# "name of function": [ 
# (res1, res2, ...), # expected outputs of first test
# (res3, res4, ...), # expexted outputs of second test
# ...
# ]
# 
# Or: "name of variable": value_expected
expecteds = {
    "foo": [
        (pd.DataFrame(data=[[2, 3], ['cats', 'dogs']])),
        (pd.DataFrame(data=[[10, 51, 0], ["parrots", "rabbits", "wolfs"]])),
    ],
    "foo_var": 42,
    "selector_advertisement": [
        pd.DataFrame([items.iloc[0], item_to_promote])
    ],
    "selector_tastes": ['image_4049.png', 'image_5742.jpg'],
}


def test(fct):
    for inp, exp in zip(inputs[fct.__name__], expecteds[fct.__name__]):
        actual = fct(**inp)
        if actual.__repr__() != exp.__repr__():
            print("❌ Tests failed... =(")
            print("--------------------------------------")
            print("On input:")
            for k, v in inp.items():
                print(f"{k}:")
                display(v)
                print()
            print("------")
            print(f"Expected:")
            display(exp)
            print(f"Actual:")
            display(actual)
            print("--------------------------------------")
            return
    print("🆗 Tests passed ! =)") 
    
def test_values(var, var_name):
    assert var == expecteds[var_name], "❌ Test failed... =("