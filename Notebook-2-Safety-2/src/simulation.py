import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import time
from huggingface_hub import HfFileSystem
from PIL import Image


# Loading the dataset
memes = pd.read_csv("dataset/labels_mapped.csv", index_col="image_name")
categories = memes.columns

# add a column "temp" to help for calculations
# memes["temp"] = 0
# add an author for each image
memes["author"] = np.random.choice(
    [
        "Alice",
        "Bob",
        "Charlie",
        "Chad",
        "David",
        "Eve",
        "Franck",
        "GigaChad",
        "Heidi",
        "Ivan",
        "Judy",
        "Kurt",
        "Leander",
        "Ludovic",
        "Mike",
        "Niaj",
        "Olivia",
        "Pat",
        "Quentin",
        "Roman",
        "Sybil",
        "Ted",
        "An Unnamed cell",
        "Vanna",
        "Walter",
        "Xx_D4rkL0rd_xX",
        "Yohan",
        "Zakarias",
    ],
    len(memes),
)
memes.at["image_0.png", "author"] = "Celebrations"

# creates a random number generator at load time (once) -- global variable used in several functions below
rng = np.random.default_rng(0)

fs = HfFileSystem()


def show_meme(meme):
    with fs.open(f"datasets/RS-Course/Memes-Sw2/images/{meme}", "rb") as f:
        display(Image.open(f))


def random_name():
    """
    Return a random name
    """
    names = [
        "Alice",
        "Bob",
        "Charlie",
        "Chad",
        "David",
        "Eve",
        "Franck",
        "GigaChad",
        "Heidi",
        "Ivan",
        "Judy",
        "Kurt",
        "Leander",
        "Ludovic",
        "Mike",
        "Niaj",
        "Olivia",
        "Pat",
        "Quentin",
        "Roman",
        "Sybil",
        "Ted",
        "An Unnamed cell",
        "Vanna",
        "Walter",
        "Xx_D4rkL0rd_xX",
        "Yohan",
        "Zakarias",
    ]
    return rng.choice(names, 1)[0]


### ------------------------- Users
def generate_users(nb):
    """
    Generate users with random tastes
    :param nb: number of users
    :param rng: default_rng of numpy
    :return: dataframe containing the users
    """
    data = []
    for i in range(nb):
        tastes = rng.random(len(categories)) * 2 - 1
        data += [[random_name()] + tastes.tolist()]
    df = pd.DataFrame(data, columns=["name"] + categories.tolist())
    return df


def choose(k):
    """
    Return the rank of selected item, follows a beta distribution with parameters (0.5, 3).
    result in about chosing the 1st item 25% of the time, 10% the 2nd, then 7%, 6%, 5%, etc...
    :param rng: default_rng of numpy
    :param k: number of items
    :return: rank of selected item
    """
    return int(rng.beta(0.5, 3) * k)


def update_tastes(user, item, trace, step):
    """
    Compute the next tastes of the user with respect to described formula, after he see the given item
    Also add the taste in the trace.
    """
    trace[step] = user.values
    error = rng.normal(0, 0.15)
    return (user + 0.025 * (0.5 + error + user @ item) * (item - user)).clip(-1, 1)


### ------------------------- Simulation
def simulation(users, items, selector, trace, nb_steps=100, k=5):
    """
    Simulate the usage of the application by the users
    :param selector: (items, user, rng, k)=> DataFram of items
    """
    for step in range(nb_steps):
        for idn, row in users.iterrows():
            slate = selector(
                items, users.iloc[idn, 1:], k
            )  # selector must return a dataframe
            item = slate.iloc[choose(k)]
            users.iloc[idn, 1:] = update_tastes(
                users.iloc[idn, 1:], item[categories], trace[idn], step
            )


def simulate_and_render(users, memes, selector, col, nb_steps=100, k=5, custom=None):
    """
    Reset the users to their original tastes, simulate their behavior and plot the results
    :param selector: recommendation system: select k items from the items given a user
    :param col: (default: (0, 1)) columns of the tastes to plot
    :param nb_steps: (default: 100) number of steps the simulation is run.
    :param k: (default: 5) number of items in a slate
    :param custom: (default: None) custom plotting instructions added
    """
    # Wow you are really interested in the implementation of the simulation ? Awesome curiosity of yours :)
    # Reset the users before simulation
    users = users.copy()

    # unit tests
    if len(col) != 2:
        raise Exception("Invalid number of columns specified")
    for idx, row in users.iterrows():
        res = selector(memes, row[1:], k)
        if type(res) != pd.DataFrame:
            raise Exception("Incorrect format of the slate: dataframe expected")
        if res.shape[0] != k:
            raise Exception(
                f"Incorrect length of the slate: {k} elements expected (k), got {len(res)}"
            )

    # trace initialisation
    trace = [np.zeros((nb_steps, len(categories))) for _ in range(len(users))]

    simulation(users, memes, selector, trace, nb_steps, k)
    plot_trace(trace, col, custom)


def custom_items_plotting(memes, cols, nb_items):
    samples = memes.sample(nb_items)
    for i, row in samples.iterrows():
        plt.plot(row[cols[0]], row[cols[1]], marker="o", color=(0.1, 0.1, 0.1, 0.1))


def plot_item_to_promote(item_to_promote, cols):
    plt.plot(
        item_to_promote[cols[0]], item_to_promote[cols[1]], marker="*", markersize=15
    )


def plot_trace(trace, cat, custom=False):
    """
    Plot the evolution tastes of the users over time.
    The graph shows the path the tastes took in the space of the attributes with lines
    :param col: list of the 2 columns of the tastes to plot (2 maximum)
    """
    for user in trace:
        plt.plot(
            user[:, categories.to_list().index(cat[0])],
            user[:, categories.to_list().index(cat[1])],
            marker=",",
        )
        plt.plot(
            user[:, categories.to_list().index(cat[0])][-1],
            user[:, categories.to_list().index(cat[1])][-1],
            marker="+",
        )
    plt.xlabel(cat[0])
    plt.ylabel(cat[1])
    plt.xlim(-1.25, 1.25)
    plt.ylim(-1.25, 1.25)
    if custom:
        custom()
    plt.grid(True)
    plt.show()


### ------------------------- Toy selector
def basic_selector(items, user, k):
    return items.sample(k)


### ------------------------- Nowhere to be safe exercise
def obfuscation(a, b, c, d, e):
    x = pd.DataFrame(np.zeros((1, len(categories))), columns=categories)
    x[r([97, 118, 118, 107, e, 119], d)] = r([77, 98, 110, 111, e, 119, 127], d)
    a = pd.concat([a, pd.concat([x] * c(10), ignore_index=True)])
    a.index = a.index[: a.shape[0] - c(10)].tolist() + [
        r([34, 117, 116, 114, 112, 113, 52, 113, 120, 110, 113, 45], d)
    ] * c(10)
    b = generate_users(30)
    return a, b


def verification(answer):
    # Did you really think it would be that simple ? <_<
    return hashing(answer) == 4095504120


def r(a, b):
    # This is not a computer security course, stop digging ! ￣へ￣
    p = []
    for i in range(len(a)):
        p += [b(a[i] - i)]
    return "".join(p)


def hashing(text):
    h = 0
    for ch in text:
        h = (h * 45 ^ ord(ch) * 991) & 0xFFFFFFFF
    return h
