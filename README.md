# CS-290 Responsible Software

This repository contains the programming exercises of the EPFL course [CS-290 Responsible Software](https://www.epfl.ch/education/educational-initiatives/responsible-software/) for academic year 2024-2025. It will be updated regularly throughout the semester.


## Overview

| Module | Topic |
|------|---------|
| 0 | Introduction: Python and ethical dilemmas |
| 1 | Safety 1: Content moderation |
| 2 | Safety 2: Content recommendation |
| 3 | Fairness 1: University admissions |
| 4 | Fairness 2: Predicting recidivism |
| 5 | Sustainability 1: Carbon footprint |
| 6 | Sustainability 2: Embodied impacts |
| 7 | Empowerment 1: Automation bias |
| 8 | Empowerment 2: Explainability methods |


## Install Python $\geqslant$ 3.10
If you don't want to use [Noto](https://noto.epfl.ch/) but prefer to run the notebooks on your local machine, you will need to install Python. This procedure uses Visual Studio Code to run the notebooks but feel free to search for other options.

1. Download and install [Visual Studio Code](https://code.visualstudio.com/).
2. Download and install Python.
   - Windows: We recommand using the [Windows Store](https://apps.microsoft.com/detail/9nrwmjp3717k?hl=en-us&gl=US)
   - MacOS: Download [brew](https://brew.sh/) and run `brew install python` in the terminal.
   - Linux: Run `sudo apt-get update && sudo apt-get install python3` in the terminal.
3. Clone the repository to your local machine.
4. Create a virtual environment by running just outside the repository folder:
   ```bash
   python -m venv responsible-software
   # Windows
   # .responsible-software\Scripts\activate
   # macOS/Linux
   source responsible-software/bin/activate
   # To check if the virtual environment is activated
   which python
   # You should see the path to the virtual environment
   # Navigate to the repository folder
   cd cs-290-responsible-software
   python3 -m pip install --upgrade pip
   python3 -m pip install -r requirements.txt
   ```
5. Open Visual Studio Code and go to the Extensions tab (Ctrl+Shift+X) or for macOS (Cmd+Shift+X).
6. Search for the `Python` and `Jupyter` extensions and install them.
7. Open the repository in Visual Studio Code.
8. Open the file `interpreter.py` and click on the Python version on the bottom left corner or Select Python Interpreter
9. Click on `Open Interpreter Path...`
10. Paste the path to the virtual environment you created in step 4. To get the path run:
    ```bash
    # navigate to the virtual environment named responsible-software or whatever you named it
    # on Linux and macOS
    pwd
    # on Windows
    # echo %cd%
    ```
11. Open the notebook you want to run.
12. On the top right corner, click `Select Kernel` then `Python Environments...` refresh the list with the arow on the top right corner and select the venv you created.
13. You are all set! We recommand to do the Introduction notebook first to get familiar with Python.

## License
Except where otherwise noted, the content of the notebooks in this repository is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution International License</a> (CC BY 4.0 International).<br/>
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0;margin-top:10px;" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>