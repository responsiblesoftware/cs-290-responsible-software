from otter.test_files import test_case

OK_FORMAT = False

name = "emissions_translation"
points = None

@test_case(points=None, hidden=False)
def test_translate_emissions(translate_emissions, math):
    result = translate_emissions(100)
    assert math.isclose(result["CO2_kg"][0], 100), f"Expected 100 but got {result['CO2_kg'][0]}"
    assert math.isclose(result["car_km"][0], 610), f"Expected 610 but got {result['car_km'][0]}"
    assert math.isclose(result["plane_km"][0], 538), f"Expected 538 but got {result['plane_km'][0]}"
    assert math.isclose(result["rail_km"][0], 22422), f"Expected 22422 but got {result['rail_km'][0]}"
    
