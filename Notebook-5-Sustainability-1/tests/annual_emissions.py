from otter.test_files import test_case

OK_FORMAT = False

name = "annual_emissions"
points = None

@test_case(points=None, hidden=False)
def test_mean_registrations_per_year(mean_registrations_per_year, mean_registrations_per_year_rounded, math):
    assert math.isclose(mean_registrations_per_year, 3076030.9166666665), "Incorrect value for mean_registrations_per_year."
    assert math.isclose(mean_registrations_per_year_rounded, 3076031.0), "Incorrect value for mean_registrations_per_year_rounded."
    
@test_case(points=None, hidden=False)
def test_emissions_per_call(emissions_per_call, math):
    assert math.isclose(emissions_per_call, 0.0008294697498152), f"Expected emissions_per_call to be 0.0008294697498152, but got {emissions_per_call}"
    
@test_case(points=None, hidden=False)
def test_annual_emissions(annual_emissions, math):
    assert math.isclose(annual_emissions, 2551.4746639938), f"Expected {annual_emissions} to be 2551.4746639938"
    
@test_case(points=None, hidden=False)
def test_calculate_annual_emissions(
    calculate_annual_emissions, registrations, emissions_per_call, math
):
    assert math.isclose(
        calculate_annual_emissions(registrations, emissions_per_call), 2551.4746639938
    ), f"Expected {calculate_annual_emissions(registrations, emissions_per_call)} to be 2551.4746639938"


