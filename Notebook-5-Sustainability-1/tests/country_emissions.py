from otter.test_files import test_case

OK_FORMAT = False

name = "country_emissions"
points = None

@test_case(points=None, hidden=False)
def test_emissions_by_country_sorted(emissions_by_country, emissions_by_country_sorted):
    assert emissions_by_country_sorted.equals(emissions_by_country.sort_values('Emissions')), "emissions_by_country_sorted is not correct"
    
@test_case(points=None, hidden=False)
def test_countries_with_lowest_emissions(emissions_by_country, top_ten_countries_with_lowest_emissions):
    assert top_ten_countries_with_lowest_emissions.equals(emissions_by_country.sort_values('Emissions').head(10)), "countries_with_lowest_emissions is not correct"
    
@test_case(points=None, hidden=False)
def test_annual_emissions_KEN(annual_emissions_KEN, math):
    assert math.isclose(annual_emissions_KEN, 1195.6919534353888), f"Expected {annual_emissions_KEN} to be 1195.6919534353888"
    
