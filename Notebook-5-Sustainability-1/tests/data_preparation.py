from otter.test_files import test_case

OK_FORMAT = False

name = "data_preparation"
points = None

@test_case(points=None, hidden=False)
def model_input_is_correct(model_input, pd):
    model_input_ref = pd.read_csv('res/model_input.csv')
    assert model_input.equals(model_input_ref), "The model_input DataFrame is not correct. Make sure you included all the necessary columns and that the order is the same as in the law_school_data DataFrame."
@test_case(points=None, hidden=False)
def model_exp_output_is_correct(model_exp_output, pd):
    model_exp_output_ref = pd.read_csv('res/model_exp_output.csv')
    assert model_exp_output == model_exp_output_ref['Passed bar exam'].to_list(), "The model_exp_output list is not correct. Make sure you included the correct column."
    
