from otter.test_files import test_case

OK_FORMAT = False

name = "data_collection"
points = None

@test_case(points=None, hidden=False)
def test_n_entries(n_entries):
    assert n_entries == 20798, "n_entries is not correct"
    
