from otter.test_files import test_case

OK_FORMAT = False

name = "chat_gpt"
points = None

@test_case(points=None, hidden=False)
def yearly_emissions_are_correct(yearly_emissions_chatgpt, math):
    assert math.isclose(
        yearly_emissions_chatgpt, 7016.028
    ), f"Expected {yearly_emissions_chatgpt} to be 7016.028"
