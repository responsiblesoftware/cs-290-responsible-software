import random
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score


sample_course_list = [
    [
        "".join(
            random.choice("abcdefghijklmnopqrstuvwxyz1234567890") for i in range(10)
        ),
        random.randint(1, 1000),
    ]
    for i in range(20000)
]


def print_emissions_translation(emissions):
    print(
        "{:,} kg of CO₂eq are equivalent to:\n🚙 driving an average passenger car for {:,} km,\n🛫 taking an international flight for {:,} km or\n🚂 travelling by train for {:,} km.".format(
            emissions.at[0, "CO2_kg"],
            emissions.at[0, "car_km"],
            emissions.at[0, "plane_km"],
            emissions.at[0, "rail_km"],
        ).replace(',', ' ')
    )


def train_model(algorithm, train_data, train_output):
    """
    Trains a machine learning model using the specified algorithm.

    Parameters:
    algorithm (str): The algorithm to use for training the model.
    train_data (array-like): The training data.
    train_output (array-like): The target variable for training.

    Returns:
    trained_model: The trained machine learning model.
    """
    if algorithm == "Logistic regression":
        model = LogisticRegression(max_iter=50, random_state=42, solver="newton-cg")
    elif algorithm == "Random forest":
        model = RandomForestClassifier(
            max_depth=5, n_estimators=100, max_features=5, random_state=42
        )
    elif algorithm == "K-nearest neighbors":
        model = KNeighborsClassifier(n_neighbors=3)

    trained_model = model.fit(train_data, train_output)

    return trained_model


def eval_model(model, eval_data, eval_output):
    """
    Evaluates the performance of a given model.

    Parameters:
    model (object): The trained model object.
    eval_data (array-like): The evaluation data.
    eval_output (array-like): The true output values for the evaluation data.

    Returns:
    float: The accuracy, i.e. the fraction of correctly classified samples in the evaluation data.
    """
    predicted_output = model.predict(eval_data)
    accuracy = accuracy_score(eval_output, predicted_output)
    return accuracy

class AlgorythmOptimizationTest:
    def __init__(self, perf_counter, optimized_function, data):
        self.data = data
        self.optimized_function = optimized_function
        self.perf_counter = perf_counter
        self.output_optimized = None
        self.output_slow = None
        self.coef = 0
        self.correct = False
        self.coef_enough = 2
        self.coef_medium = 3.5
        self.coef_fast = 5
        self.coef_bonus = 10

    def slow_function(self, data):
        """Finds and returns the course of the input DataFrame with the largest number of enrolled students. If two or more courses have the same number of students, the first one in alphabetical order is returned.

        Args:
            course_data (Pandas DataFrame): A DataFrame with two columns: `course_id` and `number of students`

        Returns:
            list: A list containing the id of the course with the most students and the number of students enrolled in it.
        """
        # create a list of all courses included in the DataFrame
        course_list = (
            data.values.tolist()
        )  # 2-dimensional list of courses of the form [[course_id, number_of_students], ...]
        course_list_size = len(course_list)
        # iterate over all courses, comparing each one with those that come after it in the list
        for i in range(course_list_size):
            max_i = i  # index of the course with the maximum number of students
            for j in range(i + 1, course_list_size):
                # select the index of the course with the maximum number of students between the two that are compared in every iteration
                if course_list[j][1] > course_list[max_i][1]:
                    max_i = j  # update the index of the course with the maximum number of students
            # swap the courses to sort the list
            (course_list[i], course_list[max_i]) = (
                course_list[max_i],
                course_list[i],
            )

        # if two or more courses have the same number of students, find the first one alphabetically
        max_n_students = course_list[0][1]
        first_alphabetically = course_list[0][0]
        most_popular_course = course_list[0]
        # iterate over all courses to find the first one alphabetically among those with the maximum number of students
        for i in range(course_list_size):
            # if the number of students of the course is equal to the maximum number of students, compare the course with the first one alphabetically and update the most popular course
            if (
                course_list[i][1] == max_n_students
                and course_list[i][0] < first_alphabetically
            ):
                first_alphabetically = course_list[i][0]
                most_popular_course = course_list[i]

        return most_popular_course

    def test_correctness(self):
        self.correct = self.output_optimized == self.output_slow

    def test_optimized_function(self):
        start = self.perf_counter()
        self.output_optimized = self.optimized_function(self.data)
        end = self.perf_counter()
        return end - start

    def test_slow_function(self):
        start = self.perf_counter()
        self.output_slow = self.slow_function(self.data)
        end = self.perf_counter()
        return end - start

    def test(self, n=1):
        self.coef = 0
        for i in range(n):
            time_optimized = self.test_optimized_function()
            time_slow = self.test_slow_function()
            self.test_correctness()
            self.coef += time_slow / time_optimized

        self.coef /= n
        
        if not self.correct:
            print(
                f"Your function is not correct. Optimized function returned {self.output_optimized} while slow function returned {self.output_slow}."
            )
        else:
            print("Your function is correct.")

        if self.coef > 1:
            print(
                f"Optimized function is {self.coef:.2f} times faster than the slow function."
            )
        else:
            print(
                f"Optimized function is {1/self.coef:.2f} times slower than the slow function. Check the optimized function."
            )

    def test_enough(self):
        assert self.coef > self.coef_enough and self.correct

    def test_medium(self):
        assert self.coef > self.coef_medium and self.correct

    def test_fast(self):
        assert self.coef > self.coef_fast and self.correct

    def test_bonus(self):
        assert self.coef > self.coef_bonus and self.correct
