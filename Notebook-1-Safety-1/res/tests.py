import pandas as pd
pd.set_option('display.width', 1000)
pd.set_option('display.max_colwidth', 150)
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# "name of function": [
# {"arg1": value1, "arg2": value2}, # input of first test
# {"arg1": value3, "arg2": value4}, # input of second test
# ...
# ]
inputs = {
    "foo": [
        {"col1": [2, 3], "col2": ['cats', 'dogs'], "patrick": None},
        {"col1": [10, 51, 0], "col2": ["parrots", "rabbits", "wolfs"]},
    ],
    "has_banned_word": [
        {"sentence_to_scan": "list of things", "banned_word": "thing"},
        {"sentence_to_scan": "list of things", "banned_word": "THING"},
        {"sentence_to_scan": "list of things", "banned_word": "things"},
        {"sentence_to_scan": "list of things", "banned_word": "thong"},
    ],
    "has_word_from_list": [
        {"sentence_to_scan": "this is a list of things dogs can't say", "list_of_words": ["dog", "thing"]},
        {"sentence_to_scan": "this is a list of things dogs can't say", "list_of_words": ["dog", "thing"]},
        {"sentence_to_scan": "this is a list of things dogs can't say", "list_of_words": ["lettuce", "food"]},       
    ],
    "is_negative_statement": [
        {"statement": "I love computer science"},
        {"statement": "I hate computer science"}
    ],
    "compute_confusion_matrix": [
        {'comparisons': pd.DataFrame({'prediction': [0, 0, 1, 1], 'label': [0, 1, 0, 1]})},
        {'comparisons': pd.DataFrame({'prediction': [0, 1, 1, 0, 0, 1, 1], 'label': [0, 0, 1, 0, 0, 1, 0]})},
    ],
    "false_positive_rate": [
        {'TP':227, 'FP':483, 'TN':137, 'FN':13},
        {'TP':162, 'FP':81, 'TN':539, 'FN':78},
    ],
    "false_negative_rate": [
        {'TP':227, 'FP':483, 'TN':137, 'FN':13},
        {'TP':162, 'FP':81, 'TN':162, 'FN':78},
    ],
}

# "name of function": [ 
# (res1, res2, ...), # expected outputs of first test
# (res3, res4, ...), # expexted outputs of second test
# ...
# ]
expecteds = {
    "foo": [
        (pd.DataFrame(data=[[2, 3], ['cats', 'dogs']])),
        (pd.DataFrame(data=[[10, 51, 0], ["parrots", "rabbits", "wolfs"]])),
    ],
    "has_banned_word": [
        (True), (True), (True), (False),
    ],
    "has_word_from_list": [
        (True), (True), (False),
    ],
    "is_negative_statement": [
        (False), (True),
    ],
    "compute_confusion_matrix": [
        (1, 1, 1, 1), (2, 2, 3, 0)
    ],
    "false_positive_rate": [
        (483/620), (81/620)
    ],
    "false_negative_rate": [
        (13/240), (78/240)
    ],
}

def test(fct):
    for inp, exp in zip(inputs[fct.__name__], expecteds[fct.__name__]):
        actual = fct(**inp)
        if actual.__repr__() != exp.__repr__():
            print("❌ Tests failed... =(")
            print("--------------------------------------")
            print("On input:")
            for k, v in inp.items():
                print(f"{k}:")
                display(v)
                print()
            print("------")
            print(f"Expected:")
            display(exp)
            print(f"Actual:")
            display(actual)
            print("--------------------------------------")
            return
    print("🆗 Tests passed ! =)") 
    
    
def draw_confusion_matrix(TP, FP, TN, FN):
    cm = np.array([[TP, FN], [FP, TN]])
    annotations = pd.DataFrame([[f"True positives:\n{TP}", f"False negatives:\n{FN}"], [f"False positives:\n{FP}", f"True negatives:\n{TN}"]])
    g = sns.heatmap(data=cm, annot=annotations, fmt="s", cmap="viridis")
    g.set_xlabel("Predicted Labels")
    g.set_ylabel("Actual Labels")
    g.xaxis.set_label_position('top') 
    g.set_title("Confusion Matrix")
    g.set_xticks([0.5, 1.5], labels=[1, 0])
    g.set_yticks([0.5, 1.5], labels=[1, 0])
    g.xaxis.tick_top()
